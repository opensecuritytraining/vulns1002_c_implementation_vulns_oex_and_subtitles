## Static code analysis tools

Here again, the lack of strong interprocedural analysis tends to mean that static analysis tools do not detect race conditions particularly well. For instance in the [SATE V Report: Ten Years of Static Analysis Tool Expositions](https://nvlpubs.nist.gov/nistpubs/SpecialPublications/NIST.SP.500-326.pdf) report, for CWE-366 "Race conditions within a thread", none of the tools caught any of the issues in the 36 test cases. But when one one drills down into the test cases for CWE-367 TOCTOU for instance, one finds that they're all the POSIX filesystem access type races which we've said are out of scope for this class.

In summary, don't expect commercial static analysis tools to help significantly to find race conditions.