# Fix

The fixed version (6.1.6) of the VirtualBox sources is available here: [https://download.virtualbox.org/virtualbox/6.1.6/VirtualBox-6.1.6.tar.bz2](https://download.virtualbox.org/virtualbox/6.1.6/VirtualBox-6.1.6.tar.bz2). You will need to diff the files yourself.

As always, you should ask yourself: is it sufficient? Did they miss a corner case? Is there some other code path that can yield the same core bug?