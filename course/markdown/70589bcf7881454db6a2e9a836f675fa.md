## Links from slides:

Wikipedia [Race Conditions](https://en.wikipedia.org/wiki/Race_condition).  

"Identifying and Exploiting Windows Kernel Race Conditions via Memory Access Patterns", 2013, Mateusz Jurczyk, Gynvael Coldwind, [Slides](https://j00ru.vexillium.org/slides/2013/syscan.pdf), [Whitepaper](https://j00ru.vexillium.org/papers/2013/bochspwn.pdf)