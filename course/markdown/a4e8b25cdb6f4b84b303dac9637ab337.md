## Flaw

This has been [CVE-2014-0196](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2014-0196) as stumbled upon by a random Suse Linux customer in production.

You can read a 3rd party advisory describing the problem [here](https://blog.includesecurity.com/2014/06/exploiting-cve-2014-0196-a-walk-through-of-the-linux-pty-race-condition-poc/) by Samuel Groß [@5aelo](https://twitter.com/5aelo). The advisory describes the [public PoC](http://bugfuzz.com/stuff/cve-2014-0196-md.c) by Matthew Daley as well.

# Exploitation

If you will be learning exploitation from OST2 or elsewhere in the future, it is recommended to not read too much from the advisory about how this can be exploited. Rather, hold it in reserve, and later on you can try to exploit it yourself. Then, if you can't, you can treat the details in the advisory as a hint for how to go about doing the exploitation, while attempting to write an exploit yourself.