# Code

Checkout the vulnerable code as follows:

```
git clone https://gitlab.com/qemu-project/qemu.git qemu_CVE-2020-29443

cd qemu_CVE-2020-29443

git checkout 915976bd98a9286efe6f2e573cb4f1360603adf9
```

---

**Hint 0:** The attack surface is ATAPI command handling, as performed by the virtual ATAPI controller.


Now go hunt in the code for an info leak vuln.