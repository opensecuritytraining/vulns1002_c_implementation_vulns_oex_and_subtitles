[CVE-2022-22253](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-22253) - [https://labs.taszk.io/blog/post/78_hw_hwlog_race/](https://labs.taszk.io/blog/post/78_hw_hwlog_race/) - Huawei linux kernel logging customizations

[CVE-2022-44563](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-44563) - [https://labs.taszk.io/blog/post/80_update_toctou/](https://labs.taszk.io/blog/post/80_update_toctou/) - Huawei firmware update

2021 CVE None - [https://i.blackhat.com/USA21/Wednesday-Handouts/us-21-Breaking-Secure-Bootloaders.pdf](https://i.blackhat.com/USA21/Wednesday-Handouts/us-21-Breaking-Secure-Bootloaders.pdf) - Qualcomm bootloader *manufactured* 🦾 TOCTOU, created by a separate buffer overflow, in order to ease exploitation. That's right, just like how we can have intrinstic vs. manufactured OOB-Rs like we talked about in Vulns1001 (and intrinstic vs. manufactured info leaks, like we'll talk about later in this class), an attacker can also manufacture a TOCTOU situation, if it's to their advantage.

[CVE-2021-30955](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-30955) - [https://web.archive.org/web/20220315041034/https://www.cyberkl.com/cvelist/cvedetail/24](https://web.archive.org/web/20220315041034/https://www.cyberkl.com/cvelist/cvedetail/24) - Apple XNU kernel

[CVE-2019-6205](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-6205) - [https://googleprojectzero.blogspot.com/2019/04/splitting-atoms-in-xnu.html](https://googleprojectzero.blogspot.com/2019/04/splitting-atoms-in-xnu.html) - Apple XNU kernel

[CVE-2019-0797](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-0797) - [https://securelist.com/cve-2019-0797-zero-day-vulnerability/89885/](https://securelist.com/cve-2019-0797-zero-day-vulnerability/89885/) - Windows kernel 0day 🌚

[CVE-2019-0636](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-0636) - [https://i.blackhat.com/USA-19/Wednesday/us-19-Wu-Battle-Of-Windows-Service-A-Silver-Bullet-To-Discover-File-Privilege-Escalation-Bugs-Automatically.pdf](https://i.blackhat.com/USA-19/Wednesday/us-19-Wu-Battle-Of-Windows-Service-A-Silver-Bullet-To-Discover-File-Privilege-Escalation-Bugs-Automatically.pdf) - Windows kernel

[CVE-2018-2844](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-2844) - [https://www.voidsecurity.in/2018/08/from-compiler-optimization-to-code.html](https://www.voidsecurity.in/2018/08/from-compiler-optimization-to-code.html) - Oracle VirtualBox - Interesting because it's a ***compiler-caused TOCTOU***! Not something you could find through source inspection, only through binary inspection!

[CVE-2018-8611](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-8611) - [https://securelist.com/zero-day-in-windows-kernel-transaction-manager-cve-2018-8611/89253/](https://securelist.com/zero-day-in-windows-kernel-transaction-manager-cve-2018-8611/89253/) - Windows kernel 0day 🌚

[CVE-2016-9381](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-9381) - [http://xenbits.xen.org/xsa/advisory-197.html](http://xenbits.xen.org/xsa/advisory-197.html) - QEMU / Xen ***compiler-caused double-fetches***! Not something you could find through source inspection, only through binary inspection!