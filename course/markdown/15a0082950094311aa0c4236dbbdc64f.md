## Address Sanitizer (ASan)


As [discussed](https://studio.p.ost2.fyi/container/block-v1:OpenSecurityTraining2+Vulns1001_C-derived+2022_v1+type@vertical+block@79e375297eb2458f85ea88bf5be384b4) in Vulns1001, ASan is primarily useful for detecting overflow type vulnerabilites. If we think of attackers as using UDA as a means to the ends of unlocking these basic overflow and OOB-W type vulns, then ASan can detect that side effect, if not the root cause. This would still mean a developer would need to backtrace to the root cause to provide sufficient initialization to remediate the issue though.

As the rest of the vulnerability types that we'll see in this class are similarly often used to achieve memory corruptions, we'll refrain from repeating this point of "ASan can help, if and only if the bug ends up manifesting as an overflow", for the remainder of the sections, and just direct you to the detection summary table.