# Code

Checkout the vulnerable code as follows:

```
git clone https://gitlab.com/qemu-project/qemu.git qemu_CVE-2020-14364

cd qemu_CVE-2020-14364

git checkout 202d69a715a4b1824dcd7ec1683d027ed2bae6d3
```

---

**Hint 0:** The attack surface is USB packet handling, as performed by the virtual UHCI controller.


Now go hunt in the code for an UDA vuln.