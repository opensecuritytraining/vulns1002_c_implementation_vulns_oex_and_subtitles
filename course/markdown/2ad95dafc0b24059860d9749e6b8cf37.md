If you would like to examine the fixed source code to confirm the vulnerability was eliminated, you can do

```
cd xnu_CVE-2020-27932

git checkout xnu-7195.50.7.100.1
```


Or download from:  

[https://github.com/apple-oss-distributions/xnu/archive/refs/tags/xnu-7195.50.7.100.1.tar.gz](https://github.com/apple-oss-distributions/xnu/archive/refs/tags/xnu-7195.50.7.100.1.tar.gz)

