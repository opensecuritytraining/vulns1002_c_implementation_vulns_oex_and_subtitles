1
00:00:00,780 --> 00:00:05,160
so what are use after free

2
00:00:02,280 --> 00:00:08,340
vulnerabilities well it's when you use

3
00:00:05,160 --> 00:00:09,960
data after it's been freed and replaced

4
00:00:08,340 --> 00:00:12,480
with ACID

5
00:00:09,960 --> 00:00:14,820
so it's also known by the name stale

6
00:00:12,480 --> 00:00:16,260
pointers or dangling pointers and we're

7
00:00:14,820 --> 00:00:18,119
going to go with dangling pointers in

8
00:00:16,260 --> 00:00:20,580
this class because it provides so much

9
00:00:18,119 --> 00:00:22,920
better analogies so I want to show you a

10
00:00:20,580 --> 00:00:25,080
simple example sort of akin to the

11
00:00:22,920 --> 00:00:27,779
uninitialized data access from earlier

12
00:00:25,080 --> 00:00:29,460
in the class so we've got our stack over

13
00:00:27,779 --> 00:00:31,920
here and we've got our heap over here

14
00:00:29,460 --> 00:00:35,640
and I want you to follow the bouncing

15
00:00:31,920 --> 00:00:37,620
star so first thing malloc buf1 that

16
00:00:35,640 --> 00:00:40,860
fills in the local variable buf1 with

17
00:00:37,620 --> 00:00:43,020
a pointer somewhere in the heap then

18
00:00:40,860 --> 00:00:45,600
buf1 address assigned to local

19
00:00:43,020 --> 00:00:48,899
variable I so I points at the same

20
00:00:45,600 --> 00:00:53,039
location somewhere on the heap then we

21
00:00:48,899 --> 00:00:56,280
free buf1 boom and it got destroyed

22
00:00:53,039 --> 00:00:58,379
so now all of a sudden these pointers

23
00:00:56,280 --> 00:01:01,020
right here are what we term dangling

24
00:00:58,379 --> 00:01:03,660
pointers because they still absolutely

25
00:01:01,020 --> 00:01:05,640
are pointing to this memory address but

26
00:01:03,660 --> 00:01:07,200
it has been freed and it's still gray

27
00:01:05,640 --> 00:01:08,760
because we didn't fill anything in we

28
00:01:07,200 --> 00:01:11,580
never initialized it never touched it

29
00:01:08,760 --> 00:01:14,340
but it is you know disappeared it is now

30
00:01:11,580 --> 00:01:16,500
no longer available for use by this code

31
00:01:14,340 --> 00:01:18,479
so someone else somewhere else on the

32
00:01:16,500 --> 00:01:20,640
system could be using that memory of

33
00:01:18,479 --> 00:01:22,560
course again this is just super trivial

34
00:01:20,640 --> 00:01:24,360
example so there's no parallelism or

35
00:01:22,560 --> 00:01:27,720
anything but use your imagination

36
00:01:24,360 --> 00:01:29,159
pretend it was kernel code instead so

37
00:01:27,720 --> 00:01:30,600
anyways there's pointers pointing

38
00:01:29,159 --> 00:01:32,280
somewhere in the heap the code should

39
00:01:30,600 --> 00:01:34,439
never ever touch that data in the heap

40
00:01:32,280 --> 00:01:37,680
and if it does that's a use after free

41
00:01:34,439 --> 00:01:39,900
bug so then we have null being assigned

42
00:01:37,680 --> 00:01:42,240
to buf1 so great that dangling

43
00:01:39,900 --> 00:01:44,460
pointer just went away now everything is

44
00:01:42,240 --> 00:01:46,920
safe with respect to that but there was

45
00:01:44,460 --> 00:01:49,979
this pointer alias that is pointing at

46
00:01:46,920 --> 00:01:52,200
the same place and if a write occurs if

47
00:01:49,979 --> 00:01:55,200
0x1337 is written to

48
00:01:52,200 --> 00:01:58,680
that still dangling pointer then all of a

49
00:01:55,200 --> 00:02:01,140
sudden it is used this pointer it is

50
00:01:58,680 --> 00:02:02,700
used this memory after it has been freed

51
00:02:01,140 --> 00:02:04,320
and that is the nature of the

52
00:02:02,700 --> 00:02:06,600
vulnerability so if someone else is

53
00:02:04,320 --> 00:02:08,520
using memory and all of a sudden some

54
00:02:06,600 --> 00:02:09,959
code goes off writing to memory that

55
00:02:08,520 --> 00:02:12,720
it's not supposed to have access to

56
00:02:09,959 --> 00:02:15,840
that's going to cause problems so let's

57
00:02:12,720 --> 00:02:17,879
talk about the Sword of Damocles so it's

58
00:02:15,840 --> 00:02:19,800
a story of back in the day this guy

59
00:02:17,879 --> 00:02:22,140
right here Damocles is talking to

60
00:02:19,800 --> 00:02:24,120
this guy right here the king Dionysus

61
00:02:22,140 --> 00:02:27,540
and he says man it's got to be good to

62
00:02:24,120 --> 00:02:29,640
be king all this luxury and wealth and

63
00:02:27,540 --> 00:02:31,440
Dionysus says oh yeah it's great to be

64
00:02:29,640 --> 00:02:32,879
king you want to be king for a day let

65
00:02:31,440 --> 00:02:35,580
me show you what it's like to be king

66
00:02:32,879 --> 00:02:38,520
and so Damocles is like oh yeah

67
00:02:35,580 --> 00:02:41,040
definitely want to be king Dionysus sets

68
00:02:38,520 --> 00:02:44,340
him up as king and he can see all the

69
00:02:41,040 --> 00:02:46,140
luxuries and the fineries but Dionysus

70
00:02:44,340 --> 00:02:48,480
having pissed off a bunch of people in

71
00:02:46,140 --> 00:02:50,400
order to become king knows that he is

72
00:02:48,480 --> 00:02:54,239
always under the threat of violent

73
00:02:50,400 --> 00:02:58,080
usurption and so he arranged for a sword

74
00:02:54,239 --> 00:03:00,959
to be hung over the throne suspended by

75
00:02:58,080 --> 00:03:03,060
a single thread of horse hair thus

76
00:03:00,959 --> 00:03:05,760
representing the mortal peril that such

77
00:03:03,060 --> 00:03:08,459
rulers always face so you can't exactly

78
00:03:05,760 --> 00:03:11,040
enjoy all of your fun and wealth when

79
00:03:08,459 --> 00:03:12,360
you've got the ever looming danger of

80
00:03:11,040 --> 00:03:15,720
death

81
00:03:12,360 --> 00:03:18,000
so I want you to feel about dangling

82
00:03:15,720 --> 00:03:20,879
pointers the same way that Damocles does

83
00:03:18,000 --> 00:03:23,099
because at the end of the day isn't the

84
00:03:20,879 --> 00:03:26,040
Sword of Damocles really just the

85
00:03:23,099 --> 00:03:27,300
dangling pointer of Damocles yes it was

86
00:03:26,040 --> 00:03:30,300
the fact that it was dangling

87
00:03:27,300 --> 00:03:33,060
precariously looming over him that made

88
00:03:30,300 --> 00:03:35,159
it perilous and that's how I want you to

89
00:03:33,060 --> 00:03:37,260
feel about dangling pointers

90
00:03:35,159 --> 00:03:39,780
but Polly the paranoid programming

91
00:03:37,260 --> 00:03:42,239
pirate parrot says get out from

92
00:03:39,780 --> 00:03:45,900
underneath dangling pointers fight back

93
00:03:42,239 --> 00:03:48,540
cross swords by always setting pointers

94
00:03:45,900 --> 00:03:52,019
to null after you free them of course

95
00:03:48,540 --> 00:03:54,180
look out for pointer aliases mateys

96
00:03:52,019 --> 00:03:58,140
and then of course the nerd emoji has to

97
00:03:54,180 --> 00:04:01,680
speak up and says yes but if the problem

98
00:03:58,140 --> 00:04:04,019
is a race condition based use after free

99
00:04:01,680 --> 00:04:06,780
then that may not be sufficient to set

100
00:04:04,019 --> 00:04:08,819
the pointer to null and while the emoji

101
00:04:06,780 --> 00:04:10,860
is correct as usual

102
00:04:08,819 --> 00:04:12,360
that doesn't mean you shouldn't set the

103
00:04:10,860 --> 00:04:13,860
pointers to null after you free them

104
00:04:12,360 --> 00:04:15,299
that just means you need to actually pay

105
00:04:13,860 --> 00:04:17,940
attention to whether or not you're under

106
00:04:15,299 --> 00:04:19,859
threat of races so why would you be

107
00:04:17,940 --> 00:04:22,139
under threat of race well maybe if the

108
00:04:19,859 --> 00:04:23,759
pointer was a shared resource

109
00:04:22,139 --> 00:04:26,100
so let's imagine you've got kernel

110
00:04:23,759 --> 00:04:28,320
thread running along it frees some

111
00:04:26,100 --> 00:04:30,479
pointer foo and so foo automatically

112
00:04:28,320 --> 00:04:33,000
becomes a dangling pointer then a

113
00:04:30,479 --> 00:04:35,940
context switch occurs and some attacker

114
00:04:33,000 --> 00:04:38,040
influenced thread is running and that

115
00:04:35,940 --> 00:04:40,080
thread writes to the dangling pointer

116
00:04:38,040 --> 00:04:42,360
because that thread is in code that has

117
00:04:40,080 --> 00:04:44,639
a use after free vulnerability well it's

118
00:04:42,360 --> 00:04:46,560
the fact that that was a shared resource

119
00:04:44,639 --> 00:04:48,660
shared between these multiple threads

120
00:04:46,560 --> 00:04:51,540
the fact that you know if there's

121
00:04:48,660 --> 00:04:53,160
parallelism as there is in kernels that

122
00:04:51,540 --> 00:04:56,100
you know anything can preempt be

123
00:04:53,160 --> 00:04:57,720
preempted at any time well then that's a

124
00:04:56,100 --> 00:04:59,220
situation for race conditions and you

125
00:04:57,720 --> 00:05:01,500
have to be concerned with it because

126
00:04:59,220 --> 00:05:03,840
when a context switches back and it sets

127
00:05:01,500 --> 00:05:05,520
the foo equal to null yes you can cross

128
00:05:03,840 --> 00:05:07,080
swords with the foo and yes you can get

129
00:05:05,520 --> 00:05:10,740
rid of that dangling pointer and that's

130
00:05:07,080 --> 00:05:13,020
good but because of this parallelism and

131
00:05:10,740 --> 00:05:14,880
shared resource you had a race condition

132
00:05:13,020 --> 00:05:16,020
that still meant even though the

133
00:05:14,880 --> 00:05:18,000
programmer was doing the right thing

134
00:05:16,020 --> 00:05:20,940
over here and freeing and setting to

135
00:05:18,000 --> 00:05:22,440
null well too bad so sad there was a

136
00:05:20,940 --> 00:05:24,419
race condition as well

137
00:05:22,440 --> 00:05:26,160
so again this doesn't mean you shouldn't

138
00:05:24,419 --> 00:05:28,020
set your pointers to null immediately

139
00:05:26,160 --> 00:05:29,580
after freeing them that just means be

140
00:05:28,020 --> 00:05:31,139
aware of race conditions right that's

141
00:05:29,580 --> 00:05:32,580
why we taught you about race conditions

142
00:05:31,139 --> 00:05:34,440
before this section

143
00:05:32,580 --> 00:05:36,720
so what are some common causes for use

144
00:05:34,440 --> 00:05:39,300
after frees well the first one is if

145
00:05:36,720 --> 00:05:41,220
the attacker can take and influence an

146
00:05:39,300 --> 00:05:43,680
ACID pointer something they fully

147
00:05:41,220 --> 00:05:45,180
control and pass that into free well

148
00:05:43,680 --> 00:05:46,979
then they essentially have an arbitrary

149
00:05:45,180 --> 00:05:48,960
free and they can cause memory to just

150
00:05:46,979 --> 00:05:50,880
disappear out from underneath anyone

151
00:05:48,960 --> 00:05:53,639
which is going to cause a problem when

152
00:05:50,880 --> 00:05:56,039
someone accesses that memory later on if

153
00:05:53,639 --> 00:05:58,500
they can fill in that memory with ACID

154
00:05:56,039 --> 00:06:00,720
another reason is when you have a

155
00:05:58,500 --> 00:06:02,639
premature free usually caused by

156
00:06:00,720 --> 00:06:04,620
something like a race condition and

157
00:06:02,639 --> 00:06:06,840
those race condition based premature

158
00:06:04,620 --> 00:06:09,120
frees are often caused by reference

159
00:06:06,840 --> 00:06:11,340
counting so that's one of our words of

160
00:06:09,120 --> 00:06:13,860
power so if you see reference count used

161
00:06:11,340 --> 00:06:16,259
it's not always going to be in something

162
00:06:13,860 --> 00:06:18,479
like C++ it could be in kernel

163
00:06:16,259 --> 00:06:21,660
code that handles garbage collection

164
00:06:18,479 --> 00:06:23,460
automatically so it's the use garbage

165
00:06:21,660 --> 00:06:25,680
collection mechanisms just generally

166
00:06:23,460 --> 00:06:27,240
expect to have reference counts which

167
00:06:25,680 --> 00:06:28,680
they use in order to figure out when

168
00:06:27,240 --> 00:06:30,419
there's no more references to memory

169
00:06:28,680 --> 00:06:32,520
great I can free it

170
00:06:30,419 --> 00:06:34,979
and then of course there's always logic

171
00:06:32,520 --> 00:06:37,080
bugs which can do things like double

172
00:06:34,979 --> 00:06:39,720
frees or accidentally freeing something

173
00:06:37,080 --> 00:06:41,220
before and just literally just have a

174
00:06:39,720 --> 00:06:43,740
logic bug where they put a free in there

175
00:06:41,220 --> 00:06:45,660
somewhere and then they access the data

176
00:06:43,740 --> 00:06:48,120
and maybe under normal circumstances

177
00:06:45,660 --> 00:06:50,880
that's totally fine because the data as

178
00:06:48,120 --> 00:06:53,220
we've said doesn't actually change when

179
00:06:50,880 --> 00:06:55,440
it's freed and so maybe the code is fine

180
00:06:53,220 --> 00:06:57,600
but if an attacker can then influence

181
00:06:55,440 --> 00:06:59,280
that data to become ACID then the code's

182
00:06:57,600 --> 00:07:02,400
not going to be fine

183
00:06:59,280 --> 00:07:04,860
so let's look at the visualization of

184
00:07:02,400 --> 00:07:08,220
some of these types so AC free C

185
00:07:04,860 --> 00:07:10,979
attacker-controlled free see so in an AC

186
00:07:08,220 --> 00:07:13,020
free C the legitimate code has a pointer

187
00:07:10,979 --> 00:07:15,300
to its data which is going to become the

188
00:07:13,020 --> 00:07:18,600
victim data on the heap and the attacker

189
00:07:15,300 --> 00:07:20,880
induces the calling of free on an ACID

190
00:07:18,600 --> 00:07:22,620
pointer so they've got a pointer and

191
00:07:20,880 --> 00:07:25,560
they can point it wherever they want if

192
00:07:22,620 --> 00:07:27,740
it's an ACID pointer and they just cause

193
00:07:25,560 --> 00:07:30,120
the freeing of this victim data

194
00:07:27,740 --> 00:07:32,520
consequently this legitimate pointer

195
00:07:30,120 --> 00:07:35,039
becomes a dangling pointer and if the

196
00:07:32,520 --> 00:07:37,500
attacker can cause ACID to be filled in

197
00:07:35,039 --> 00:07:39,360
at that freed location then any

198
00:07:37,500 --> 00:07:41,699
subsequent use of the dangling pointer

199
00:07:39,360 --> 00:07:43,139
means that this legitimate code is going

200
00:07:41,699 --> 00:07:46,800
to get burnt

201
00:07:43,139 --> 00:07:47,819
another type was that racey free C so

202
00:07:46,800 --> 00:07:49,440
let's say that you've got some

203
00:07:47,819 --> 00:07:51,479
legitimate code interacting with other

204
00:07:49,440 --> 00:07:53,819
legitimate code it's got pointer to this

205
00:07:51,479 --> 00:07:56,160
data and then some more interactions

206
00:07:53,819 --> 00:07:59,039
occur some more pointers are pointing to

207
00:07:56,160 --> 00:08:01,259
this apparently shared resource and

208
00:07:59,039 --> 00:08:03,120
these threads are sharing but then the

209
00:08:01,259 --> 00:08:05,160
attacker maliciously interacts with

210
00:08:03,120 --> 00:08:07,860
legitimate code in such a way as to

211
00:08:05,160 --> 00:08:10,860
cause an erroneous free and then all of

212
00:08:07,860 --> 00:08:13,620
a sudden boom that disappears and these

213
00:08:10,860 --> 00:08:15,660
pointers become dangling pointers and

214
00:08:13,620 --> 00:08:17,819
once again if the attacker can fill in

215
00:08:15,660 --> 00:08:20,220
ACID there and cause any sort of

216
00:08:17,819 --> 00:08:23,039
utilization of a dangling pointer well

217
00:08:20,220 --> 00:08:25,560
the good code is going to get burned and

218
00:08:23,039 --> 00:08:28,139
then finally double free C

219
00:08:25,560 --> 00:08:31,560
so this is a situation where there's a

220
00:08:28,139 --> 00:08:33,060
bug that causes two instances of free on

221
00:08:31,560 --> 00:08:34,800
the same pointer

222
00:08:33,060 --> 00:08:36,180
so it's a malicious code might interact

223
00:08:34,800 --> 00:08:38,880
with the buggy code and it's got this

224
00:08:36,180 --> 00:08:41,640
pointer P at the original data and it

225
00:08:38,880 --> 00:08:44,039
causes a free of p and this is the first

226
00:08:41,640 --> 00:08:46,560
of the two frees and then boom that

227
00:08:44,039 --> 00:08:49,080
disappears now free is a dangling

228
00:08:46,560 --> 00:08:50,940
pointer then the malicious code maybe

229
00:08:49,080 --> 00:08:53,279
interacts with some other code and says

230
00:08:50,940 --> 00:08:55,740
hey you should really you know create an

231
00:08:53,279 --> 00:08:58,200
object right now that object will get

232
00:08:55,740 --> 00:09:01,080
created in the gap in the heap that the

233
00:08:58,200 --> 00:09:03,839
attacker had presumably heap feng shui to

234
00:09:01,080 --> 00:09:05,519
their way into making and then they

235
00:09:03,839 --> 00:09:08,820
interact with the buggy code again and

236
00:09:05,519 --> 00:09:10,980
cause the second free and then all of a

237
00:09:08,820 --> 00:09:14,040
sudden this victim data is going to

238
00:09:10,980 --> 00:09:16,740
disappear and the victim unknown to it

239
00:09:14,040 --> 00:09:19,800
is holding on to what is in reality a

240
00:09:16,740 --> 00:09:21,959
dangling pointer so as always if the

241
00:09:19,800 --> 00:09:23,760
attacker can fill in ACID there and

242
00:09:21,959 --> 00:09:26,279
cause the utilization of a dangling

243
00:09:23,760 --> 00:09:29,160
pointer that's going to burn the code

244
00:09:26,279 --> 00:09:32,940
now I want to make one important caveat

245
00:09:29,160 --> 00:09:34,680
about this double free there is a CWE

246
00:09:32,940 --> 00:09:36,839
for a thing called a double free

247
00:09:34,680 --> 00:09:39,740
vulnerability and that is what I would

248
00:09:36,839 --> 00:09:43,140
call the traditional double free so

249
00:09:39,740 --> 00:09:45,120
traditionally there is a situation where

250
00:09:43,140 --> 00:09:48,240
if you call free on the same pointer

251
00:09:45,120 --> 00:09:51,180
twice because of the background behavior

252
00:09:48,240 --> 00:09:53,940
of the actual heap itself this can cause

253
00:09:51,180 --> 00:09:56,820
corruption of heap metadata and that can

254
00:09:53,940 --> 00:09:59,279
lead to exploitable situations so I want

255
00:09:56,820 --> 00:10:02,399
to be very clear this double free in the

256
00:09:59,279 --> 00:10:04,500
context of use after frees is not the

257
00:10:02,399 --> 00:10:06,959
same thing as a double free we're just

258
00:10:04,500 --> 00:10:09,660
freeing the same pointer twice just

259
00:10:06,959 --> 00:10:11,700
automagically causes heap corruption to

260
00:10:09,660 --> 00:10:13,560
the attacker's advantage that's a nice

261
00:10:11,700 --> 00:10:15,420
vulnerability but we haven't covered

262
00:10:13,560 --> 00:10:16,920
that in this class partially because

263
00:10:15,420 --> 00:10:18,720
it's the kind of thing that can just get

264
00:10:16,920 --> 00:10:20,760
mitigated away you fix the heap

265
00:10:18,720 --> 00:10:22,500
implementation and it's fixed for

266
00:10:20,760 --> 00:10:24,600
everyone everywhere who's using that

267
00:10:22,500 --> 00:10:25,860
heap so it's appropriate and it's in

268
00:10:24,600 --> 00:10:28,080
scope it's something we'll probably talk

269
00:10:25,860 --> 00:10:29,640
about in future classes but it's always

270
00:10:28,080 --> 00:10:31,320
going to be very operating system

271
00:10:29,640 --> 00:10:33,480
dependent it's not a generic thing

272
00:10:31,320 --> 00:10:35,580
whereas this is potentially a generic

273
00:10:33,480 --> 00:10:37,860
thing anytime you can find a bug that

274
00:10:35,580 --> 00:10:39,360
can allow you to induce two frees on the

275
00:10:37,860 --> 00:10:41,040
same thing even if the heap

276
00:10:39,360 --> 00:10:42,540
implementation has been hardened and

277
00:10:41,040 --> 00:10:44,880
it's not causing a traditional

278
00:10:42,540 --> 00:10:46,860
double-free vulnerability you can still

279
00:10:44,880 --> 00:10:48,899
potentially place the play these games

280
00:10:46,860 --> 00:10:50,459
in order to cause use after free

281
00:10:48,899 --> 00:10:53,279
vulnerabilities

282
00:10:50,459 --> 00:10:55,620
so we could have given you some some

283
00:10:53,279 --> 00:10:57,720
trivial code examples to reinforce these

284
00:10:55,620 --> 00:10:59,459
but I tried starting to write some

285
00:10:57,720 --> 00:11:01,620
trivial code examples and they got

286
00:10:59,459 --> 00:11:04,260
non-trivial very fast so rather than

287
00:11:01,620 --> 00:11:07,459
focusing on toy examples let's go look

288
00:11:04,260 --> 00:11:07,459
at some real examples

