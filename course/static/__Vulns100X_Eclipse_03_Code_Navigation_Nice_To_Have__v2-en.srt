1
00:00:00,599 --> 00:00:04,620
um but let's see some of the other nice

2
00:00:02,220 --> 00:00:07,440
to have commands as well so the first

3
00:00:04,620 --> 00:00:09,960
one is go to a matching bracer bracket

4
00:00:07,440 --> 00:00:13,440
so again if you're in like some giant

5
00:00:09,960 --> 00:00:14,820
function or particularly a giant uh if

6
00:00:13,440 --> 00:00:17,220
statement or something like that or

7
00:00:14,820 --> 00:00:19,619
you've got nested like five different

8
00:00:17,220 --> 00:00:20,939
four and while loops and an if statement

9
00:00:19,619 --> 00:00:22,500
that kind of thing you're trying to

10
00:00:20,939 --> 00:00:24,840
figure out like okay where is this

11
00:00:22,500 --> 00:00:28,619
particular thing bounded well that is

12
00:00:24,840 --> 00:00:32,279
where Ctrl shift p will help you jump

13
00:00:28,619 --> 00:00:33,840
back and forth between uh between braces

14
00:00:32,279 --> 00:00:35,399
for instance so let's say I'm way back

15
00:00:33,840 --> 00:00:36,540
at the bottom of the function I want to

16
00:00:35,399 --> 00:00:39,420
just quickly get to the top of the

17
00:00:36,540 --> 00:00:42,180
function Ctrl shift p will get me back

18
00:00:39,420 --> 00:00:45,059
to the matching brace

19
00:00:42,180 --> 00:00:48,059
then we have next occurrence of selected

20
00:00:45,059 --> 00:00:50,039
text command K this can be useful when

21
00:00:48,059 --> 00:00:51,780
you're trying to so you can see that the

22
00:00:50,039 --> 00:00:52,860
nice thing about again what this is one

23
00:00:51,780 --> 00:00:55,620
of the kind of things that I like about

24
00:00:52,860 --> 00:00:57,660
Eclipse if you just select a variable it

25
00:00:55,620 --> 00:01:01,079
automatically highlights stuff for you

26
00:00:57,660 --> 00:01:03,059
like this right so tracking ACID can be

27
00:01:01,079 --> 00:01:05,640
very easy here because you can basically

28
00:01:03,059 --> 00:01:07,140
say like okay well where is this used oh

29
00:01:05,640 --> 00:01:08,880
I just click on it and then I can like

30
00:01:07,140 --> 00:01:10,799
see quickly in the overview like

31
00:01:08,880 --> 00:01:12,479
everywhere it's used I can just kind of

32
00:01:10,799 --> 00:01:14,939
skim down with my eyeballs through the

33
00:01:12,479 --> 00:01:15,720
code to see where a value that's ACID is

34
00:01:14,939 --> 00:01:18,060
used

35
00:01:15,720 --> 00:01:20,159
but if you want to like if you're in

36
00:01:18,060 --> 00:01:22,320
bigger code and it's not all visible on

37
00:01:20,159 --> 00:01:24,780
a single page and you want to like move

38
00:01:22,320 --> 00:01:27,060
to the next occurrence of something this

39
00:01:24,780 --> 00:01:29,580
is where the find next occurrence of

40
00:01:27,060 --> 00:01:31,619
selected text is useful and so here it's

41
00:01:29,580 --> 00:01:33,540
not good enough to just uh you know

42
00:01:31,619 --> 00:01:35,280
click on something and see that it's you

43
00:01:33,540 --> 00:01:37,020
know been highlighted like this you have

44
00:01:35,280 --> 00:01:38,040
to actually select the text that you

45
00:01:37,020 --> 00:01:40,799
want to use

46
00:01:38,040 --> 00:01:42,960
then you're going to use command K and

47
00:01:40,799 --> 00:01:45,780
That'll Walk you forward to the next

48
00:01:42,960 --> 00:01:47,700
instance so there there now you can see

49
00:01:45,780 --> 00:01:49,439
maybe there's another one beyond my

50
00:01:47,700 --> 00:01:52,020
scope maybe there's not only one way to

51
00:01:49,439 --> 00:01:54,840
find out and there you go just keep

52
00:01:52,020 --> 00:01:57,000
moving forward to all usage again super

53
00:01:54,840 --> 00:01:59,340
useful for tracking asset through a

54
00:01:57,000 --> 00:02:00,840
program and if you walk forwards for a

55
00:01:59,340 --> 00:02:02,939
while and you want to walk backwards to

56
00:02:00,840 --> 00:02:04,860
say hey wait what did it do earlier then

57
00:02:02,939 --> 00:02:07,799
you can just add a shift as a modifier

58
00:02:04,860 --> 00:02:10,800
and walk backwards with command shift k

59
00:02:07,799 --> 00:02:13,620
so nice and easy

60
00:02:10,800 --> 00:02:15,120
okay another useful thing is this

61
00:02:13,620 --> 00:02:17,099
toggling of folding elements so we're

62
00:02:15,120 --> 00:02:20,340
going to have three things about folding

63
00:02:17,099 --> 00:02:22,080
uh things right here and so I told you

64
00:02:20,340 --> 00:02:24,660
earlier in the configuration that we

65
00:02:22,080 --> 00:02:27,540
were going to set under the preferences

66
00:02:24,660 --> 00:02:30,420
folding

67
00:02:27,540 --> 00:02:33,120
and we did that and we said by default I

68
00:02:30,420 --> 00:02:36,000
want this to say that we can fold these

69
00:02:33,120 --> 00:02:39,060
ifs and elves and do walls and also the

70
00:02:36,000 --> 00:02:40,739
prep processor definitions now the thing

71
00:02:39,060 --> 00:02:43,200
is sometimes if you forget and then you

72
00:02:40,739 --> 00:02:44,819
add it later then you're not going to

73
00:02:43,200 --> 00:02:47,940
see these kind of things this is the

74
00:02:44,819 --> 00:02:49,440
what the actual folding GUI indicator

75
00:02:47,940 --> 00:02:51,120
looks like this says like this is a

76
00:02:49,440 --> 00:02:53,760
thing that you can fold you can see that

77
00:02:51,120 --> 00:02:55,620
it just hides all of this right but if

78
00:02:53,760 --> 00:02:58,500
you added that later it may not actually

79
00:02:55,620 --> 00:02:59,700
appear so the thing that I frequently

80
00:02:58,500 --> 00:03:01,140
have to do either because I've

81
00:02:59,700 --> 00:03:03,300
encountered bugs or just because I

82
00:03:01,140 --> 00:03:06,180
forget and I add it later there's the

83
00:03:03,300 --> 00:03:09,480
toggling for folding which is command

84
00:03:06,180 --> 00:03:12,060
and then the Divide key on a numpad so

85
00:03:09,480 --> 00:03:13,560
yes unfortunately to use these folding

86
00:03:12,060 --> 00:03:16,620
things you have to actually have a

87
00:03:13,560 --> 00:03:18,780
keyboard that has a numpad either that

88
00:03:16,620 --> 00:03:20,640
or you're going to just remap the keys

89
00:03:18,780 --> 00:03:22,319
right if you're interested in using this

90
00:03:20,640 --> 00:03:25,260
capability you can always just use the

91
00:03:22,319 --> 00:03:28,080
GUI or remap the keys to remap the keys

92
00:03:25,260 --> 00:03:31,260
you would go to window preferences type

93
00:03:28,080 --> 00:03:33,599
keys and then this is the place that you

94
00:03:31,260 --> 00:03:35,459
can actually remap keys so

95
00:03:33,599 --> 00:03:38,300
if I search for folding for instance

96
00:03:35,459 --> 00:03:38,300
oops

97
00:03:38,700 --> 00:03:43,140
software updates or something

98
00:03:41,099 --> 00:03:45,120
search for folding toggle folding and

99
00:03:43,140 --> 00:03:46,799
it's saying it's control numpad divide

100
00:03:45,120 --> 00:03:48,000
you could set it to something completely

101
00:03:46,799 --> 00:03:51,659
different

102
00:03:48,000 --> 00:03:53,640
but if I do that control numpad divide

103
00:03:51,659 --> 00:03:55,860
then you'll see that actually those

104
00:03:53,640 --> 00:03:58,440
little GUI indicators went away

105
00:03:55,860 --> 00:04:00,180
and so when I do Ctrl numpad divide

106
00:03:58,440 --> 00:04:01,739
again they come back and so I just

107
00:04:00,180 --> 00:04:05,099
frequently have to do this when I was

108
00:04:01,739 --> 00:04:07,799
dealing with like macro definitions and

109
00:04:05,099 --> 00:04:10,140
stuff like that because for whatever

110
00:04:07,799 --> 00:04:12,120
reason you know I encountered bugs I'm

111
00:04:10,140 --> 00:04:14,340
going to Quick cut the video to go find

112
00:04:12,120 --> 00:04:15,840
some code that looks more like the kind

113
00:04:14,340 --> 00:04:18,000
of stuff that you would be concerned

114
00:04:15,840 --> 00:04:20,699
with this kind of thing for okay here's

115
00:04:18,000 --> 00:04:22,560
some code that is macroing out something

116
00:04:20,699 --> 00:04:25,320
based on whether or not something's

117
00:04:22,560 --> 00:04:27,780
defined and the key point here is that

118
00:04:25,320 --> 00:04:30,000
Eclipse has decided based on looking at

119
00:04:27,780 --> 00:04:32,100
all the files everywhere that it does

120
00:04:30,000 --> 00:04:34,500
not think that this thing is actually

121
00:04:32,100 --> 00:04:36,000
defined so this is again one of the

122
00:04:34,500 --> 00:04:38,400
things I like about Eclipse it is

123
00:04:36,000 --> 00:04:40,380
showing this in dark gray to indicate to

124
00:04:38,400 --> 00:04:42,060
you hey I don't think this code is

125
00:04:40,380 --> 00:04:43,380
actually in play here now of course

126
00:04:42,060 --> 00:04:45,600
it'll all depend on you know whether

127
00:04:43,380 --> 00:04:47,400
there's compiler options at compile time

128
00:04:45,600 --> 00:04:49,380
that get added in that ad defines and

129
00:04:47,400 --> 00:04:50,820
stuff like that but it's just saying as

130
00:04:49,380 --> 00:04:52,860
best it can tell right now through

131
00:04:50,820 --> 00:04:55,139
static analysis this is not going to be

132
00:04:52,860 --> 00:04:57,360
defined so again we can do control

133
00:04:55,139 --> 00:04:58,860
numpad join us to hide this kind of

134
00:04:57,360 --> 00:05:01,320
stuff that we just straight up don't

135
00:04:58,860 --> 00:05:04,440
want to see because it has no relevance

136
00:05:01,320 --> 00:05:06,840
but now returning back to that notion of

137
00:05:04,440 --> 00:05:08,820
what if you knew that it actually did

138
00:05:06,840 --> 00:05:10,860
have relevance and Eclipse was just

139
00:05:08,820 --> 00:05:13,080
wrong about this the way that you would

140
00:05:10,860 --> 00:05:15,600
solve that problem globally for the

141
00:05:13,080 --> 00:05:18,479
entire workspace is you would right

142
00:05:15,600 --> 00:05:21,780
click on your project go to properties

143
00:05:18,479 --> 00:05:24,479
go to the C and C++ include

144
00:05:21,780 --> 00:05:26,520
paths and preprocessor definitions and

145
00:05:24,479 --> 00:05:27,960
then add a preprocessor definition so

146
00:05:26,520 --> 00:05:29,880
I'm just going to edit this one that's

147
00:05:27,960 --> 00:05:32,340
already here I'm going to set that equal

148
00:05:29,880 --> 00:05:34,800
to 1 so that it's just defined and when

149
00:05:32,340 --> 00:05:37,020
I do that and I apply and close now you

150
00:05:34,800 --> 00:05:39,120
can see that immediately Eclipse said

151
00:05:37,020 --> 00:05:40,320
okay yeah this is defined so this code

152
00:05:39,120 --> 00:05:42,360
is in play

153
00:05:40,320 --> 00:05:44,220
so that's just you know one of the

154
00:05:42,360 --> 00:05:45,780
things that I found very useful for some

155
00:05:44,220 --> 00:05:48,600
code bases it doesn't look like it's

156
00:05:45,780 --> 00:05:51,840
super applicable here in QEMU but for

157
00:05:48,600 --> 00:05:54,060
some code bases it's extremely essential

158
00:05:51,840 --> 00:05:57,180
all right and the final little nice to

159
00:05:54,060 --> 00:05:58,860
have command is refactor so sometimes

160
00:05:57,180 --> 00:06:02,340
you're going to get yourself into

161
00:05:58,860 --> 00:06:03,900
situations where the the code is just

162
00:06:02,340 --> 00:06:05,699
misbehaving or something and you want to

163
00:06:03,900 --> 00:06:07,560
rename it to something that makes more

164
00:06:05,699 --> 00:06:10,259
sense to you or you want to change a

165
00:06:07,560 --> 00:06:12,960
parameter just very this hasn't been

166
00:06:10,259 --> 00:06:15,419
super common occurrence for me but when

167
00:06:12,960 --> 00:06:17,400
I needed it I needed it so sometimes you

168
00:06:15,419 --> 00:06:19,740
want to refactor the code just while

169
00:06:17,400 --> 00:06:21,600
you're auditing it so I'm going to go

170
00:06:19,740 --> 00:06:24,900
back to where I came from and this is a

171
00:06:21,600 --> 00:06:26,940
good note to myself of I should have set

172
00:06:24,900 --> 00:06:29,039
a bookmark here at the beginning so just

173
00:06:26,940 --> 00:06:31,740
as a GUI thing to show you that I don't

174
00:06:29,039 --> 00:06:33,419
have on the website if you right click

175
00:06:31,740 --> 00:06:35,220
next to some code and I would recommend

176
00:06:33,419 --> 00:06:37,560
you do this throughout the class just

177
00:06:35,220 --> 00:06:39,900
because you can get lost in the code for

178
00:06:37,560 --> 00:06:41,580
any of those sort of hint codes that are

179
00:06:39,900 --> 00:06:43,500
on the website and you need to be able

180
00:06:41,580 --> 00:06:45,960
to get back there frequently you right

181
00:06:43,500 --> 00:06:48,120
click next to the code and then you

182
00:06:45,960 --> 00:06:49,800
select add bookmark

183
00:06:48,120 --> 00:06:52,020
and you can name it whatever you want

184
00:06:49,800 --> 00:06:54,060
it's going to default to whatever this

185
00:06:52,020 --> 00:06:56,639
entire line is so you know maybe

186
00:06:54,060 --> 00:06:58,560
sometimes it's better to just make it

187
00:06:56,639 --> 00:07:00,419
simpler and say okay I know it's this

188
00:06:58,560 --> 00:07:02,639
particular function you add that

189
00:07:00,419 --> 00:07:04,259
bookmark and now that'll make it easier

190
00:07:02,639 --> 00:07:06,180
to get back here if you have the

191
00:07:04,259 --> 00:07:09,960
bookmarks window which is a little bit

192
00:07:06,180 --> 00:07:11,520
hard to find so you have to go to window

193
00:07:09,960 --> 00:07:13,440
Show view

194
00:07:11,520 --> 00:07:15,539
and then other because actually the

195
00:07:13,440 --> 00:07:17,639
bookmarks aren't used that often

196
00:07:15,539 --> 00:07:19,020
so from there then you're going to type

197
00:07:17,639 --> 00:07:22,139
in bookmark

198
00:07:19,020 --> 00:07:24,120
and select it hit open it'll default to

199
00:07:22,139 --> 00:07:26,639
being over here in the project Explorer

200
00:07:24,120 --> 00:07:28,440
but usually I like to move it somewhere

201
00:07:26,639 --> 00:07:30,539
else like

202
00:07:28,440 --> 00:07:33,479
down here that was an interesting

203
00:07:30,539 --> 00:07:36,240
virtual machine issue right there so

204
00:07:33,479 --> 00:07:38,520
yeah just move it down here and if I

205
00:07:36,240 --> 00:07:40,800
need it again oh yeah moving this thing

206
00:07:38,520 --> 00:07:42,599
doesn't like that if I need it and you

207
00:07:40,800 --> 00:07:45,060
know if I get lost in the code I've been

208
00:07:42,599 --> 00:07:46,560
like drilling down you know 10 functions

209
00:07:45,060 --> 00:07:48,120
deep and I can't figure out where I am

210
00:07:46,560 --> 00:07:50,400
anymore I just kind of back up and I say

211
00:07:48,120 --> 00:07:52,020
okay I know that I started from this

212
00:07:50,400 --> 00:07:53,880
function and I can get back there with

213
00:07:52,020 --> 00:07:55,560
my handy dandy bookmark

214
00:07:53,880 --> 00:07:57,780
but so let's say that for whatever

215
00:07:55,560 --> 00:08:00,120
reason I wanted to refactor this code

216
00:07:57,780 --> 00:08:02,039
the key thing about refactoring in this

217
00:08:00,120 --> 00:08:04,319
context is really it's just kind of like

218
00:08:02,039 --> 00:08:08,400
a find and replace on a name everywhere

219
00:08:04,319 --> 00:08:10,740
but it's sort of a semantically aware

220
00:08:08,400 --> 00:08:12,479
find and replace so it's understanding

221
00:08:10,740 --> 00:08:13,740
that if you're refactoring a variable it

222
00:08:12,479 --> 00:08:15,539
should just change the variable name

223
00:08:13,740 --> 00:08:17,880
everywhere that that is treated as an

224
00:08:15,539 --> 00:08:19,979
actual symbol if you're refactoring a

225
00:08:17,880 --> 00:08:21,979
function it should treat only it should

226
00:08:19,979 --> 00:08:25,500
change only sort of the function

227
00:08:21,979 --> 00:08:28,259
definitions and and declarations that

228
00:08:25,500 --> 00:08:31,620
kind of thing so anyways shift alt R

229
00:08:28,259 --> 00:08:35,459
here command option r on Max and we can

230
00:08:31,620 --> 00:08:37,979
do that so shift alt r

231
00:08:35,459 --> 00:08:40,560
and then it says please enter a new name

232
00:08:37,979 --> 00:08:42,120
and so you're going to Mouse around in

233
00:08:40,560 --> 00:08:45,720
here and do whatever you're going to do

234
00:08:42,120 --> 00:08:49,080
I call this init Dev ring 2.

235
00:08:45,720 --> 00:08:51,360
then oops and then I have to hit enter

236
00:08:49,080 --> 00:08:54,180
so now it should go through the code it

237
00:08:51,360 --> 00:08:57,120
should change this everywhere now if I

238
00:08:54,180 --> 00:09:00,240
find all the references with Ctrl shift

239
00:08:57,120 --> 00:09:02,399
G so find all references I see the exact

240
00:09:00,240 --> 00:09:04,860
same references as I saw before there

241
00:09:02,399 --> 00:09:06,600
were two calls to this function right

242
00:09:04,860 --> 00:09:10,019
here in this function and they've both

243
00:09:06,600 --> 00:09:11,940
been fixed up if I hit function F3 to go

244
00:09:10,019 --> 00:09:15,080
to the definition then I'm going to be

245
00:09:11,940 --> 00:09:18,120
right back where I started so again not

246
00:09:15,080 --> 00:09:21,720
extremely common and not super necessary

247
00:09:18,120 --> 00:09:23,760
in this in this particular class but it

248
00:09:21,720 --> 00:09:25,980
is a thing for you to be aware of just

249
00:09:23,760 --> 00:09:27,839
in case you need to do it and if for

250
00:09:25,980 --> 00:09:30,060
some one whatever reason you need to

251
00:09:27,839 --> 00:09:33,120
rename some stuff you know variables

252
00:09:30,060 --> 00:09:35,940
functions whatever so here you go I did

253
00:09:33,120 --> 00:09:38,700
the same thing on this TBL and maybe I

254
00:09:35,940 --> 00:09:40,380
want to call it table two or something

255
00:09:38,700 --> 00:09:43,380
like that you can see that everything

256
00:09:40,380 --> 00:09:45,839
just updated in real time down here as I

257
00:09:43,380 --> 00:09:48,600
was refactoring

258
00:09:45,839 --> 00:09:51,660
and Ctrl Z to undo

259
00:09:48,600 --> 00:09:53,880
okay so those are the most important

260
00:09:51,660 --> 00:09:55,860
commands again these are the essential

261
00:09:53,880 --> 00:09:58,740
things as far as I'm concerned these are

262
00:09:55,860 --> 00:10:00,839
like the must-have for any sort of IDE

263
00:09:58,740 --> 00:10:01,740
or code viewing environment these are

264
00:10:00,839 --> 00:10:04,140
the kind of things you're going to be

265
00:10:01,740 --> 00:10:07,080
doing over and over and over again these

266
00:10:04,140 --> 00:10:10,080
are nice tabs and you know again there's

267
00:10:07,080 --> 00:10:12,120
some niceties of eclipse in terms of its

268
00:10:10,080 --> 00:10:14,640
treatment of macros that just kind of

269
00:10:12,120 --> 00:10:17,339
put it over the top for me as a nice

270
00:10:14,640 --> 00:10:18,959
cross-platform environment from which to

271
00:10:17,339 --> 00:10:21,720
dig around in code that I can't

272
00:10:18,959 --> 00:10:24,720
necessarily compile so hopefully this is

273
00:10:21,720 --> 00:10:28,459
helpful to you as you continue to read

274
00:10:24,720 --> 00:10:28,459
the fun code throughout this class

