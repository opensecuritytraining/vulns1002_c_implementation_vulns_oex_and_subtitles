1
00:00:00,659 --> 00:00:05,700
let's learn about race conditions first

2
00:00:03,300 --> 00:00:07,980
and foremost what are they well

3
00:00:05,700 --> 00:00:09,780
according to Wikipedia a race condition

4
00:00:07,980 --> 00:00:11,940
is where the system's substantive

5
00:00:09,780 --> 00:00:15,420
behavior is dependent on the sequence or

6
00:00:11,940 --> 00:00:17,760
timing of other uncontrollable events

7
00:00:15,420 --> 00:00:20,100
well cool what is a security

8
00:00:17,760 --> 00:00:22,800
vulnerability race condition

9
00:00:20,100 --> 00:00:26,699
that is when the substantive behavior

10
00:00:22,800 --> 00:00:28,199
depends on attacker controllable events

11
00:00:26,699 --> 00:00:29,820
so there's two types of people in the

12
00:00:28,199 --> 00:00:32,040
world there are the lumpers and the

13
00:00:29,820 --> 00:00:34,440
splitters I am the latter type I like me

14
00:00:32,040 --> 00:00:37,020
a good taxonomy I like to split things

15
00:00:34,440 --> 00:00:38,340
up finely and say you know what is this

16
00:00:37,020 --> 00:00:40,980
and how is it different from something

17
00:00:38,340 --> 00:00:44,820
else so let me give you a Venn diagram

18
00:00:40,980 --> 00:00:46,140
of race conditions and a subclass of

19
00:00:44,820 --> 00:00:48,180
race condition that we're going to see

20
00:00:46,140 --> 00:00:50,760
in this class is the double fetch

21
00:00:48,180 --> 00:00:52,140
vulnerability so all double fetches are

22
00:00:50,760 --> 00:00:55,440
race conditions but not all race

23
00:00:52,140 --> 00:00:58,260
conditions are double fetches a subset

24
00:00:55,440 --> 00:01:00,300
of the double fetch vulnerability is the

25
00:00:58,260 --> 00:01:02,520
TOCTOU vulnerability time of check time

26
00:01:00,300 --> 00:01:05,939
of use that is a situation where you

27
00:01:02,520 --> 00:01:08,880
fetch once and you check something and

28
00:01:05,939 --> 00:01:11,040
then you fetch again and you use it but

29
00:01:08,880 --> 00:01:13,200
between the time that you checked it and

30
00:01:11,040 --> 00:01:14,580
you used it a race condition could have

31
00:01:13,200 --> 00:01:16,560
occurred and the attacker could have

32
00:01:14,580 --> 00:01:19,020
swapped out something that was clean at

33
00:01:16,560 --> 00:01:21,720
check time with something that is ACID

34
00:01:19,020 --> 00:01:24,659
at use time and while I like me a good

35
00:01:21,720 --> 00:01:27,720
taxonomy this isn't a taxonomy this is

36
00:01:24,659 --> 00:01:29,520
the eye of Hal who's coming to kill us

37
00:01:27,720 --> 00:01:30,960
all

38
00:01:29,520 --> 00:01:32,759
but

39
00:01:30,960 --> 00:01:34,320
when I was looking at the eye of Hal for

40
00:01:32,759 --> 00:01:37,140
a while eventually I convinced myself

41
00:01:34,320 --> 00:01:38,820
that actually there are time of check

42
00:01:37,140 --> 00:01:40,560
time of use vulnerabilities that are

43
00:01:38,820 --> 00:01:42,659
race conditions that do not involve a

44
00:01:40,560 --> 00:01:45,119
double fetch so the vast majority are

45
00:01:42,659 --> 00:01:46,979
double fetch but I convinced myself that

46
00:01:45,119 --> 00:01:49,680
you know there are some corner case type

47
00:01:46,979 --> 00:01:51,960
situations typically doing to do with

48
00:01:49,680 --> 00:01:54,720
shared memory and stuff like that where

49
00:01:51,960 --> 00:01:56,100
there isn't actually a double fetch it's

50
00:01:54,720 --> 00:01:58,079
just things can change out naturally

51
00:01:56,100 --> 00:02:00,060
behind the scenes so we're not going to

52
00:01:58,079 --> 00:02:01,680
see any of those cases in this class at

53
00:02:00,060 --> 00:02:03,840
least with the current examples at the

54
00:02:01,680 --> 00:02:05,759
time of the initial recording but I

55
00:02:03,840 --> 00:02:08,459
believe they do exist

56
00:02:05,759 --> 00:02:11,099
okay so the root causes for these race

57
00:02:08,459 --> 00:02:13,800
conditions is largely down to shared

58
00:02:11,099 --> 00:02:16,020
resources and parallelism by shared

59
00:02:13,800 --> 00:02:18,959
resources I mean things like RAM

60
00:02:16,020 --> 00:02:21,959
volatile memory and non-volatile memory

61
00:02:18,959 --> 00:02:24,599
things like SPI flash EEPROM hard drive

62
00:02:21,959 --> 00:02:26,520
for file systems stuff like that so

63
00:02:24,599 --> 00:02:28,020
between the two of those that's all the

64
00:02:26,520 --> 00:02:30,660
types of memory volatile and

65
00:02:28,020 --> 00:02:32,760
non-volatile so when you have a shared

66
00:02:30,660 --> 00:02:34,680
resource and there is parallelism in

67
00:02:32,760 --> 00:02:37,020
play you have the opportunity for race

68
00:02:34,680 --> 00:02:39,120
conditions so this can either be the

69
00:02:37,020 --> 00:02:41,640
sort of faux parallelism or fake

70
00:02:39,120 --> 00:02:43,080
parallelism which is multi-threading so

71
00:02:41,640 --> 00:02:45,360
you can imagine you have two clients

72
00:02:43,080 --> 00:02:47,400
talking to the same server two tabs

73
00:02:45,360 --> 00:02:49,860
executing JavaScript in the same browser

74
00:02:47,400 --> 00:02:52,620
to userspace threads or applications

75
00:02:49,860 --> 00:02:55,200
executing system calls in the same OS on

76
00:02:52,620 --> 00:02:57,599
a single CPU to operating systems

77
00:02:55,200 --> 00:02:59,160
running in the same hypervisor so this

78
00:02:57,599 --> 00:03:01,140
sort of thing where you have context

79
00:02:59,160 --> 00:03:02,760
switching but it's actually just

80
00:03:01,140 --> 00:03:05,040
flopping back and forth and there's not

81
00:03:02,760 --> 00:03:07,260
true parallelism going on that's like

82
00:03:05,040 --> 00:03:08,640
multi-threading sort of situation and

83
00:03:07,260 --> 00:03:09,739
then of course you can have the true

84
00:03:08,640 --> 00:03:12,239
parallelism when you have

85
00:03:09,739 --> 00:03:15,540
multi-processing and that is where

86
00:03:12,239 --> 00:03:17,940
you've got multiple CPUs so two CPU

87
00:03:15,540 --> 00:03:20,459
cores inside of the same system on a

88
00:03:17,940 --> 00:03:22,800
chip or for instance two separate chips

89
00:03:20,459 --> 00:03:24,239
that are accessing a shared bus like

90
00:03:22,800 --> 00:03:26,400
PCIe

91
00:03:24,239 --> 00:03:28,140
so I'm going to show some examples in

92
00:03:26,400 --> 00:03:30,239
this intro section where we've got the

93
00:03:28,140 --> 00:03:31,500
shared resource in the middle and so for

94
00:03:30,239 --> 00:03:33,720
instance you could have userspace

95
00:03:31,500 --> 00:03:35,400
having access to the file system and the

96
00:03:33,720 --> 00:03:38,040
kernel having access to the file system

97
00:03:35,400 --> 00:03:40,440
and if there is parallelism between them

98
00:03:38,040 --> 00:03:42,360
like userspace is running on one CPU

99
00:03:40,440 --> 00:03:44,099
the kernel is running on a different CPU

100
00:03:42,360 --> 00:03:46,080
then there's opportunities for race

101
00:03:44,099 --> 00:03:47,760
conditions to occur but it doesn't just

102
00:03:46,080 --> 00:03:49,920
have to be things like the kernel and

103
00:03:47,760 --> 00:03:51,900
userspace sharing file system they

104
00:03:49,920 --> 00:03:54,239
could also be sharing something like RAM

105
00:03:51,900 --> 00:03:56,040
there could be explicit shared memory

106
00:03:54,239 --> 00:03:58,260
regions that are set up between kernel

107
00:03:56,040 --> 00:04:00,239
and userspace this is a common paradigm

108
00:03:58,260 --> 00:04:02,040
for for instance getting data into

109
00:04:00,239 --> 00:04:04,379
kernelspace there might be a shared

110
00:04:02,040 --> 00:04:06,239
buffer and userspace writes some data

111
00:04:04,379 --> 00:04:07,799
to the shared buffer and kernelspace

112
00:04:06,239 --> 00:04:09,659
reads it

113
00:04:07,799 --> 00:04:11,519
and there's lots of vulnerabilities with

114
00:04:09,659 --> 00:04:13,739
that sort of paradigm across all the

115
00:04:11,519 --> 00:04:15,959
different operating systems when mutual

116
00:04:13,739 --> 00:04:18,000
exclusion is not properly enforced to

117
00:04:15,959 --> 00:04:19,919
avoid race conditions but it doesn't

118
00:04:18,000 --> 00:04:22,139
just have to be kernel and userspace

119
00:04:19,919 --> 00:04:24,660
sharing DRAM it could be kernel and

120
00:04:22,139 --> 00:04:26,639
firmware sharing DRAM let's imagine that

121
00:04:24,660 --> 00:04:28,800
you've got your network card and it's

122
00:04:26,639 --> 00:04:30,780
running firmware out to the side well

123
00:04:28,800 --> 00:04:33,180
literally the exact design of things

124
00:04:30,780 --> 00:04:35,400
like network interface cards is that

125
00:04:33,180 --> 00:04:37,199
there is a shared buffer in a kernel or

126
00:04:35,400 --> 00:04:39,360
a kernel driver for that particular

127
00:04:37,199 --> 00:04:42,060
network interface card is responsible

128
00:04:39,360 --> 00:04:43,680
for writing some packet data out to go

129
00:04:42,060 --> 00:04:45,900
out to the firmware and send a packet

130
00:04:43,680 --> 00:04:48,240
and the firmware reads packet data in

131
00:04:45,900 --> 00:04:49,680
and that shared DRAM is shared with

132
00:04:48,240 --> 00:04:51,720
kernel driver that then reads the

133
00:04:49,680 --> 00:04:53,220
packets in and sends them to userspace

134
00:04:51,720 --> 00:04:55,440
or kernelspace wherever they need to go

135
00:04:53,220 --> 00:04:57,360
but it doesn't just have to be kernel

136
00:04:55,440 --> 00:04:59,160
and firmware sharing DRAM they could

137
00:04:57,360 --> 00:05:01,560
also share something like non-volatile

138
00:04:59,160 --> 00:05:04,139
RAM so this would be things like the SPI

139
00:05:01,560 --> 00:05:05,699
flash chip when you see back in the 1001

140
00:05:04,139 --> 00:05:08,699
class there were a few vulnerabilities

141
00:05:05,699 --> 00:05:11,400
having to do with BIOS and one of them

142
00:05:08,699 --> 00:05:13,500
for instance was accessing the UEFI

143
00:05:11,400 --> 00:05:16,080
NVRAM variables or non-volatile

144
00:05:13,500 --> 00:05:18,120
variables so access to the same SPI

145
00:05:16,080 --> 00:05:19,740
flash chip between firmware and kernel

146
00:05:18,120 --> 00:05:21,840
can be another opportunity for race

147
00:05:19,740 --> 00:05:23,699
conditions but it doesn't just have to

148
00:05:21,840 --> 00:05:25,740
be kernel and firmware it can be other

149
00:05:23,699 --> 00:05:27,539
firmware and firmware so you can have

150
00:05:25,740 --> 00:05:29,759
two pieces of firmware that are sharing

151
00:05:27,539 --> 00:05:32,520
the same non-volatile memory and there

152
00:05:29,759 --> 00:05:34,320
can be race conditions between those

153
00:05:32,520 --> 00:05:37,199
so let's talk about those

154
00:05:34,320 --> 00:05:40,259
categorizations of race conditions that

155
00:05:37,199 --> 00:05:43,560
I gave earlier the first one is TOCTOU

156
00:05:40,259 --> 00:05:45,300
time of check time of use but at this

157
00:05:43,560 --> 00:05:47,759
point I have to warn you that if you

158
00:05:45,300 --> 00:05:49,440
stare long into the slides the slides

159
00:05:47,759 --> 00:05:51,660
also stare into you

160
00:05:49,440 --> 00:05:53,820
because I was spending way too much time

161
00:05:51,660 --> 00:05:55,380
making these slides last year and

162
00:05:53,820 --> 00:05:56,820
eventually these things looked like eyes

163
00:05:55,380 --> 00:05:58,139
to me and then I made a silly little

164
00:05:56,820 --> 00:05:59,880
animation

165
00:05:58,139 --> 00:06:02,039
so the way that we typically visualize

166
00:05:59,880 --> 00:06:04,860
race conditions is that we might have

167
00:06:02,039 --> 00:06:06,720
one userspace process and time is going

168
00:06:04,860 --> 00:06:08,880
from top to bottom and a second

169
00:06:06,720 --> 00:06:11,220
malicious userspace process with time

170
00:06:08,880 --> 00:06:12,419
going top to bottom there's some shared

171
00:06:11,220 --> 00:06:14,220
resource in the middle we don't know

172
00:06:12,419 --> 00:06:15,240
what it is could be RAM could be file

173
00:06:14,220 --> 00:06:18,479
system

174
00:06:15,240 --> 00:06:20,520
the first legitimate process reads to

175
00:06:18,479 --> 00:06:22,319
try to verify some data and it's green

176
00:06:20,520 --> 00:06:24,060
and it's clean to start with but then

177
00:06:22,319 --> 00:06:25,620
the attacker writes malicious data in

178
00:06:24,060 --> 00:06:27,660
there and then it reads and it writes

179
00:06:25,620 --> 00:06:29,880
and it reads and writes and the thing is

180
00:06:27,660 --> 00:06:32,880
at the time of those reads everything

181
00:06:29,880 --> 00:06:35,460
was clean and at the time of usage

182
00:06:32,880 --> 00:06:38,100
everything was dirty so this is a time

183
00:06:35,460 --> 00:06:40,319
of check time of use vulnerability at

184
00:06:38,100 --> 00:06:43,199
the time of check everything read out as

185
00:06:40,319 --> 00:06:45,360
non-ACID but if it does you know a

186
00:06:43,199 --> 00:06:47,520
verify function followed by an execute

187
00:06:45,360 --> 00:06:49,800
in place function then by the time that

188
00:06:47,520 --> 00:06:51,960
it starts to execute in place it has

189
00:06:49,800 --> 00:06:54,240
been filled in with ACID and the quick

190
00:06:51,960 --> 00:06:56,060
point I would make here is that we said

191
00:06:54,240 --> 00:06:58,020
that TOCTOU attacks are generally

192
00:06:56,060 --> 00:06:59,460
subclasses of double fetch

193
00:06:58,020 --> 00:07:01,319
vulnerabilities this is the first

194
00:06:59,460 --> 00:07:03,120
explicit fetch but then sort of

195
00:07:01,319 --> 00:07:05,699
implicitly here if I'm saying that it's

196
00:07:03,120 --> 00:07:07,620
executing this stuff if it's executing

197
00:07:05,699 --> 00:07:10,500
it in place that's sort of an implicit

198
00:07:07,620 --> 00:07:13,020
fetch and that implicit fetch is often

199
00:07:10,500 --> 00:07:14,460
what gets people in trouble so that's

200
00:07:13,020 --> 00:07:16,560
time of check time of use

201
00:07:14,460 --> 00:07:18,000
vulnerabilities usually a double fetch

202
00:07:16,560 --> 00:07:19,740
certainly all the examples we currently

203
00:07:18,000 --> 00:07:20,819
have in this class are double fetch

204
00:07:19,740 --> 00:07:22,440
vulnerabilities

205
00:07:20,819 --> 00:07:23,819
then we have double fetch

206
00:07:22,440 --> 00:07:25,740
vulnerabilities which need not

207
00:07:23,819 --> 00:07:27,840
necessarily be TOCTOU vulnerabilities

208
00:07:25,740 --> 00:07:30,720
and if you're saying to yourself why is

209
00:07:27,840 --> 00:07:32,639
that dog wearing a tutu I was also

210
00:07:30,720 --> 00:07:34,560
saying that to myself because again I

211
00:07:32,639 --> 00:07:35,940
made these slides last year and then

212
00:07:34,560 --> 00:07:37,919
they were originally for vulnerabilities

213
00:07:35,940 --> 00:07:39,300
1001 then we decided to make twice as

214
00:07:37,919 --> 00:07:41,639
many examples for half as many

215
00:07:39,300 --> 00:07:43,440
vulnerability types and I came back and

216
00:07:41,639 --> 00:07:44,759
I could not figure out why this dog was

217
00:07:43,440 --> 00:07:46,440
wearing a tutu

218
00:07:44,759 --> 00:07:49,560
until I said it a couple of times

219
00:07:46,440 --> 00:07:52,020
allowed to myself because that's 2-2 the

220
00:07:49,560 --> 00:07:55,680
double fetching dog yes that's the dog's

221
00:07:52,020 --> 00:07:57,840
name 2-2 so sequence diagram userspace

222
00:07:55,680 --> 00:08:00,840
process non-malicious userspace process

223
00:07:57,840 --> 00:08:03,120
malicious the first fetch by the

224
00:08:00,840 --> 00:08:06,120
non-malicious process gets a nice green

225
00:08:03,120 --> 00:08:09,120
ball that is non-ACID data then the

226
00:08:06,120 --> 00:08:11,220
attacker writes a red ACID ball into the

227
00:08:09,120 --> 00:08:14,039
shared location and then the code

228
00:08:11,220 --> 00:08:17,039
fetches again for some reason and on

229
00:08:14,039 --> 00:08:20,639
this fetch they get the ACID ball so bad

230
00:08:17,039 --> 00:08:23,280
2-2 bad dog don't double fetch all but

231
00:08:20,639 --> 00:08:25,620
you're not a bad dog you're a good dog

232
00:08:23,280 --> 00:08:27,479
it's the programmer who programmed you

233
00:08:25,620 --> 00:08:29,160
to double fetch who's bad

234
00:08:27,479 --> 00:08:31,560
so that's double fetch vulnerability

235
00:08:29,160 --> 00:08:33,539
which need not necessarily be a time of

236
00:08:31,560 --> 00:08:35,459
check time of use so let's look at an

237
00:08:33,539 --> 00:08:37,440
example quick this was some real but

238
00:08:35,459 --> 00:08:39,419
simplified code from the Windows kernel

239
00:08:37,440 --> 00:08:41,700
from a while back and so in this case

240
00:08:39,419 --> 00:08:44,039
the attacker in userspace was allowed

241
00:08:41,700 --> 00:08:46,080
to provide a pointer to a buffer in user

242
00:08:44,039 --> 00:08:48,360
space and that pointer would be stored

243
00:08:46,080 --> 00:08:50,399
in kernelspace in this buffer size and

244
00:08:48,360 --> 00:08:52,560
then they could also provide a pointer

245
00:08:50,399 --> 00:08:54,480
to the buffer itself so they'd say hey

246
00:08:52,560 --> 00:08:55,560
kernel here's my buffer and here's how

247
00:08:54,480 --> 00:08:59,580
long it is

248
00:08:55,560 --> 00:09:01,800
but these are ACID pointers and when you

249
00:08:59,580 --> 00:09:04,860
dereference that pointer you are

250
00:09:01,800 --> 00:09:09,000
fetching from DRAM where that pointer

251
00:09:04,860 --> 00:09:11,700
points so this right here is ACID being

252
00:09:09,000 --> 00:09:14,880
used for an allocation so that in and of

253
00:09:11,700 --> 00:09:17,459
itself is a bad thing and then if a race

254
00:09:14,880 --> 00:09:19,440
condition occurs so if the kernel stops

255
00:09:17,459 --> 00:09:22,019
and halts and context switches somewhere

256
00:09:19,440 --> 00:09:24,600
else and runs some other code well if

257
00:09:22,019 --> 00:09:27,120
another CPU could have some code run

258
00:09:24,600 --> 00:09:29,700
that changes out the content that's

259
00:09:27,120 --> 00:09:31,620
stored at that buffer size pointer then

260
00:09:29,700 --> 00:09:34,260
all of a sudden a completely different

261
00:09:31,620 --> 00:09:36,420
value will be used down here so an

262
00:09:34,260 --> 00:09:39,540
allocation with one size occurred here

263
00:09:36,420 --> 00:09:42,000
and a memory copy with a different size

264
00:09:39,540 --> 00:09:44,100
occurred later on consequently if that

265
00:09:42,000 --> 00:09:47,100
different size was bigger this would be

266
00:09:44,100 --> 00:09:49,260
an under allocation and this would be an

267
00:09:47,100 --> 00:09:51,120
over copy and we're right back to the

268
00:09:49,260 --> 00:09:54,779
simple buffer overflows that we learned

269
00:09:51,120 --> 00:09:57,240
about in vulnerabilities 1001.

270
00:09:54,779 --> 00:10:00,240
put another way this first pointer

271
00:09:57,240 --> 00:10:01,860
dereference is fetch one but then if the

272
00:10:00,240 --> 00:10:04,620
context switches out and the race

273
00:10:01,860 --> 00:10:07,800
condition occurs this second pointer

274
00:10:04,620 --> 00:10:10,800
dereference is fetch two and that is going

275
00:10:07,800 --> 00:10:12,480
to be a double fetch vulnerability but

276
00:10:10,800 --> 00:10:14,459
now I said in this particular case that

277
00:10:12,480 --> 00:10:16,500
this is not a TOCTOU vulnerability

278
00:10:14,459 --> 00:10:19,260
because there was essentially no check

279
00:10:16,500 --> 00:10:22,140
so there's no sanity check of any sort

280
00:10:19,260 --> 00:10:24,120
right here it's just usage one usage two

281
00:10:22,140 --> 00:10:26,760
so that's double fetching but it's not

282
00:10:24,120 --> 00:10:28,820
time of check time of use and now when I

283
00:10:26,760 --> 00:10:31,680
saw this vulnerability I kind of went

284
00:10:28,820 --> 00:10:34,140
because I know that I could write this

285
00:10:31,680 --> 00:10:36,120
kind of vulnerability very easily and so

286
00:10:34,140 --> 00:10:38,100
could you I think this sort of

287
00:10:36,120 --> 00:10:40,019
vulnerability where it's as subtle as

288
00:10:38,100 --> 00:10:42,420
just dereferencing a pointer like you

289
00:10:40,019 --> 00:10:44,519
would do very naturally all the time

290
00:10:42,420 --> 00:10:46,800
well if you're not aware of the fact

291
00:10:44,519 --> 00:10:49,200
that there is parallelism opportunities

292
00:10:46,800 --> 00:10:51,540
going on if you're running in the kernel

293
00:10:49,200 --> 00:10:53,940
for instance then you may not realize

294
00:10:51,540 --> 00:10:55,620
that just dereferencing a pointer that

295
00:10:53,940 --> 00:10:57,060
happens to point to userspace where an

296
00:10:55,620 --> 00:11:00,180
attacker could change out the value

297
00:10:57,060 --> 00:11:01,740
thanks to parallelism that dereferencing

298
00:11:00,180 --> 00:11:03,240
a pointer in and of itself is a

299
00:11:01,740 --> 00:11:05,279
dangerous operation

300
00:11:03,240 --> 00:11:07,380
so again the root causes of these race

301
00:11:05,279 --> 00:11:09,959
conditions is the combination of shared

302
00:11:07,380 --> 00:11:11,880
resources so this is the shared memory

303
00:11:09,959 --> 00:11:14,160
between userspace and kernelspace and

304
00:11:11,880 --> 00:11:16,680
the opportunity for parallelism

305
00:11:14,160 --> 00:11:19,260
so again that was not a TOCTOU that

306
00:11:16,680 --> 00:11:21,300
was a double fetch that had no check so

307
00:11:19,260 --> 00:11:22,800
here's another very similar example from

308
00:11:21,300 --> 00:11:25,019
the same researchers just the white

309
00:11:22,800 --> 00:11:27,360
paper version instead of the slides and

310
00:11:25,019 --> 00:11:28,980
so here we have a first fetch of some

311
00:11:27,360 --> 00:11:31,320
pointer that's pointing at user

312
00:11:28,980 --> 00:11:34,500
controlled address and they hopefully

313
00:11:31,320 --> 00:11:36,120
provided it in red as ACID so this is a

314
00:11:34,500 --> 00:11:38,399
dereferencing of the pointer but this is

315
00:11:36,120 --> 00:11:40,680
for a sanity check this is for something

316
00:11:38,399 --> 00:11:43,740
that's trying to avoid a buffer overflow

317
00:11:40,680 --> 00:11:45,720
down below so fetch one time of check

318
00:11:43,740 --> 00:11:48,420
explicitly being used as a security

319
00:11:45,720 --> 00:11:50,339
check and everything looks good at that

320
00:11:48,420 --> 00:11:52,980
point it's not too big but context

321
00:11:50,339 --> 00:11:55,079
switch and do the old switcheroo change

322
00:11:52,980 --> 00:11:58,680
out the content and now this second

323
00:11:55,079 --> 00:12:01,260
dereference of the value is ACID and so

324
00:11:58,680 --> 00:12:04,320
the time of use this is going to buffer

325
00:12:01,260 --> 00:12:07,860
overflow so you've got ACID you've got a

326
00:12:04,320 --> 00:12:09,779
fixed size local buffer on the stack and

327
00:12:07,860 --> 00:12:12,540
consequently that is an under allocation

328
00:12:09,779 --> 00:12:14,640
because the attacker can change out the

329
00:12:12,540 --> 00:12:17,579
size and this will be an over copy

330
00:12:14,640 --> 00:12:19,680
so just like we saw back in the integer

331
00:12:17,579 --> 00:12:21,839
overflow and underflow section we saw

332
00:12:19,680 --> 00:12:25,079
integer overflows and underflows could

333
00:12:21,839 --> 00:12:27,060
be used to bypass sanity checks and

334
00:12:25,079 --> 00:12:29,040
reopen your traditional stack and heap

335
00:12:27,060 --> 00:12:30,779
overflow so in this case you can see

336
00:12:29,040 --> 00:12:33,720
that the race condition when it's a time

337
00:12:30,779 --> 00:12:36,720
of check time of use it's reopening a

338
00:12:33,720 --> 00:12:38,640
traditional buffer overflow

339
00:12:36,720 --> 00:12:39,600
all right so that's an example of

340
00:12:38,640 --> 00:12:41,519
TOCTOU

341
00:12:39,600 --> 00:12:43,560
finally let's cover just other race

342
00:12:41,519 --> 00:12:45,959
conditions that don't fit into those two

343
00:12:43,560 --> 00:12:47,700
categories so once again back to the

344
00:12:45,959 --> 00:12:49,680
definition what is a race condition

345
00:12:47,700 --> 00:12:51,720
generically it's where the system

346
00:12:49,680 --> 00:12:53,399
substantive behavior is dependent on the

347
00:12:51,720 --> 00:12:55,320
sequence or timing of other

348
00:12:53,399 --> 00:12:57,240
uncontrollable events and we said that

349
00:12:55,320 --> 00:12:59,339
if they become attacker controllable

350
00:12:57,240 --> 00:13:01,320
events then that is where we have

351
00:12:59,339 --> 00:13:03,300
security vulnerabilities so let's again

352
00:13:01,320 --> 00:13:06,060
imagine we have a shared resource and we

353
00:13:03,300 --> 00:13:09,420
have process one writing into the shared

354
00:13:06,060 --> 00:13:11,160
resource but then just in time process 2

355
00:13:09,420 --> 00:13:14,519
writes into it as well and they write

356
00:13:11,160 --> 00:13:16,680
some ACID right before the process one

357
00:13:14,519 --> 00:13:18,779
is going to read that shared resource

358
00:13:16,680 --> 00:13:21,000
and so now they're reading back ACID

359
00:13:18,779 --> 00:13:22,860
instead of reading back clean data that

360
00:13:21,000 --> 00:13:24,540
they had written in here and this is

361
00:13:22,860 --> 00:13:27,060
very much a traditional race condition

362
00:13:24,540 --> 00:13:28,740
in the sense that it all depends on like

363
00:13:27,060 --> 00:13:31,260
when this write comes in versus when

364
00:13:28,740 --> 00:13:33,000
that write comes in so if the ordering

365
00:13:31,260 --> 00:13:35,040
was changed or if the ordering is

366
00:13:33,000 --> 00:13:37,019
variable between these and sometimes the

367
00:13:35,040 --> 00:13:38,820
attacker's write comes in first well the

368
00:13:37,019 --> 00:13:41,100
attacker doesn't win the race in that

369
00:13:38,820 --> 00:13:43,560
condition they only win the race if they

370
00:13:41,100 --> 00:13:45,899
can control the ordering or timing of

371
00:13:43,560 --> 00:13:48,839
the events to ensure that their write

372
00:13:45,899 --> 00:13:50,279
occurs after the good write and before

373
00:13:48,839 --> 00:13:52,200
the good read

374
00:13:50,279 --> 00:13:53,700
and so this is the sort of generic race

375
00:13:52,200 --> 00:13:55,760
condition anytime that there's

376
00:13:53,700 --> 00:13:58,320
variability the attacker wants to

377
00:13:55,760 --> 00:14:00,420
manipulate the events in such a way that

378
00:13:58,320 --> 00:14:01,920
they can guarantee that they will always

379
00:14:00,420 --> 00:14:03,600
win the race

380
00:14:01,920 --> 00:14:05,700
so how do you normally deal with race

381
00:14:03,600 --> 00:14:07,920
conditions well you deal with it with

382
00:14:05,700 --> 00:14:10,680
mutual exclusion you have something like

383
00:14:07,920 --> 00:14:13,139
a lock on the shared resource you lock

384
00:14:10,680 --> 00:14:16,019
it and then this attempt to write by the

385
00:14:13,139 --> 00:14:17,820
attacker is denied and later on when the

386
00:14:16,019 --> 00:14:19,920
first process is done with the resource

387
00:14:17,820 --> 00:14:21,360
it unlocks it and at that point the

388
00:14:19,920 --> 00:14:23,339
attacker can write but it doesn't matter

389
00:14:21,360 --> 00:14:25,440
because at that point the good process

390
00:14:23,339 --> 00:14:27,240
has already successfully read the clean

391
00:14:25,440 --> 00:14:30,600
data so the generic race condition

392
00:14:27,240 --> 00:14:33,740
solution is mutual exclusion mutual

393
00:14:30,600 --> 00:14:36,420
exclusion is no race condition

394
00:14:33,740 --> 00:14:39,240
improper mutual exclusion is when there

395
00:14:36,420 --> 00:14:41,279
are opportunities for race conditions

396
00:14:39,240 --> 00:14:43,019
so that was an example of a race

397
00:14:41,279 --> 00:14:45,180
condition that is neither double fetch

398
00:14:43,019 --> 00:14:47,100
nor TOCTOU attack right because they

399
00:14:45,180 --> 00:14:48,720
only fetched once it's just they wanted

400
00:14:47,100 --> 00:14:51,779
to make sure that the attacker couldn't

401
00:14:48,720 --> 00:14:53,639
manipulate that single fetch now I want

402
00:14:51,779 --> 00:14:55,199
to be clear that mutual exclusion does

403
00:14:53,639 --> 00:14:58,079
not always mean that you have to use

404
00:14:55,199 --> 00:15:00,240
mechanisms such as mutexes semaphores or

405
00:14:58,079 --> 00:15:03,180
locking primitives in the case of double

406
00:15:00,240 --> 00:15:05,699
fetch it suffices to basically just

407
00:15:03,180 --> 00:15:08,100
fetch once store it somewhere that the

408
00:15:05,699 --> 00:15:10,560
attacker can't manipulate it like a

409
00:15:08,100 --> 00:15:12,600
local variable on your stack if you know

410
00:15:10,560 --> 00:15:14,399
you're the kernel and they're userspace or

411
00:15:12,600 --> 00:15:17,399
you're some firmware and they're the

412
00:15:14,399 --> 00:15:20,040
kernel just need to store something in a

413
00:15:17,399 --> 00:15:22,680
non-manipulatable location so fetch it

414
00:15:20,040 --> 00:15:24,600
once and then use the copy use the clean

415
00:15:22,680 --> 00:15:26,820
copy that the attacker can't manipulate

416
00:15:24,600 --> 00:15:29,220
anymore you will have successfully

417
00:15:26,820 --> 00:15:32,100
achieved mutual exclusion without the

418
00:15:29,220 --> 00:15:34,139
need for things like mutexes so the big

419
00:15:32,100 --> 00:15:37,680
takeaway from this section is that

420
00:15:34,139 --> 00:15:40,320
sharing is not caring and specifically

421
00:15:37,680 --> 00:15:42,839
sharing without mutual exclusion is not

422
00:15:40,320 --> 00:15:44,279
caring because we said the core and root

423
00:15:42,839 --> 00:15:46,440
causes of these race condition

424
00:15:44,279 --> 00:15:48,660
vulnerabilities are parallelism and

425
00:15:46,440 --> 00:15:50,579
shared resources well you're probably

426
00:15:48,660 --> 00:15:53,820
not going to get rid of the parallelism

427
00:15:50,579 --> 00:15:56,040
and go back to single CPU single

428
00:15:53,820 --> 00:15:58,680
threading or anything else like that so

429
00:15:56,040 --> 00:16:00,839
parallelism is a necessary component of

430
00:15:58,680 --> 00:16:03,000
modern computing so what we have to

431
00:16:00,839 --> 00:16:06,000
focus on is making sure that those

432
00:16:03,000 --> 00:16:08,160
shared resources are accessed with

433
00:16:06,000 --> 00:16:10,860
mutual exclusion it's just recognition

434
00:16:08,160 --> 00:16:13,440
of those situations in which a resource

435
00:16:10,860 --> 00:16:16,019
could be shared and in which parallelism

436
00:16:13,440 --> 00:16:18,380
could cause that resource to change over

437
00:16:16,019 --> 00:16:18,380
time

