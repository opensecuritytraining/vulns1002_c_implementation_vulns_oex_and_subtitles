1
00:00:00,060 --> 00:00:04,259
in this video I'm going to briefly walk

2
00:00:01,920 --> 00:00:06,359
you through installation of eclipse and

3
00:00:04,259 --> 00:00:08,940
setting up an initial project I'm going

4
00:00:06,359 --> 00:00:11,160
to do this on Linux but the process is

5
00:00:08,940 --> 00:00:12,120
more or less the same on Windows and Mac

6
00:00:11,160 --> 00:00:14,340
OS

7
00:00:12,120 --> 00:00:17,760
so when you I want you to specifically

8
00:00:14,340 --> 00:00:21,119
install Eclipse 202212 also known as

9
00:00:17,760 --> 00:00:22,500
release R and so you have two options

10
00:00:21,119 --> 00:00:25,560
when you go to this page you can either

11
00:00:22,500 --> 00:00:28,080
download directly just the executable or

12
00:00:25,560 --> 00:00:29,519
you can use the installer package and I

13
00:00:28,080 --> 00:00:31,460
want you to use the installer package

14
00:00:29,519 --> 00:00:34,739
because that will also install

15
00:00:31,460 --> 00:00:36,540
prerequisite Java runtime environment so

16
00:00:34,739 --> 00:00:38,219
that you don't have to worry about if

17
00:00:36,540 --> 00:00:40,559
your system doesn't have that

18
00:00:38,219 --> 00:00:42,600
so I've already downloaded that and I

19
00:00:40,559 --> 00:00:44,460
have that available here I downloaded

20
00:00:42,600 --> 00:00:46,500
both of them but specifically the

21
00:00:44,460 --> 00:00:48,660
eclipse install installer and then

22
00:00:46,500 --> 00:00:50,340
you'll run the installer then from

23
00:00:48,660 --> 00:00:53,399
within the installer you'll select

24
00:00:50,340 --> 00:00:54,360
specifically the C/C++ developers

25
00:00:53,399 --> 00:00:57,120
option

26
00:00:54,360 --> 00:00:59,699
and then you'll install that

27
00:00:57,120 --> 00:01:02,579
just accepting the default options for

28
00:00:59,699 --> 00:01:04,760
the previous and accepting the end user

29
00:01:02,579 --> 00:01:04,760
license

30
00:01:08,520 --> 00:01:12,420
when that completes go ahead and launch

31
00:01:10,439 --> 00:01:14,460
it

32
00:01:12,420 --> 00:01:16,200
and when Eclipse first comes up it's

33
00:01:14,460 --> 00:01:18,659
going to ask you what you'd like to use

34
00:01:16,200 --> 00:01:20,820
as a workspace so you can leave the

35
00:01:18,659 --> 00:01:23,100
default for the eclipse Dash workspace

36
00:01:20,820 --> 00:01:25,500
but then we are going to create a folder

37
00:01:23,100 --> 00:01:27,119
within this location for our actual

38
00:01:25,500 --> 00:01:29,580
workspace files

39
00:01:27,119 --> 00:01:32,040
so within the class we have various

40
00:01:29,580 --> 00:01:34,020
places where we say to check out the

41
00:01:32,040 --> 00:01:36,299
vulnerable code and so in this

42
00:01:34,020 --> 00:01:40,380
particular instance I have a command to

43
00:01:36,299 --> 00:01:43,439
clone the vulnerable version of QEMU and

44
00:01:40,380 --> 00:01:46,439
I've already done that here in my VM in

45
00:01:43,439 --> 00:01:49,079
the terminal so I want you to use the

46
00:01:46,439 --> 00:01:51,479
same folder name that you ultimately are

47
00:01:49,079 --> 00:01:54,119
storing the source into and I want you

48
00:01:51,479 --> 00:01:56,040
to use that as the project workspace

49
00:01:54,119 --> 00:01:58,439
folder directory

50
00:01:56,040 --> 00:02:00,659
so I will choose that and then launch

51
00:01:58,439 --> 00:02:04,079
and it'll initialize that folder with

52
00:02:00,659 --> 00:02:05,640
some Eclipse workspace information

53
00:02:04,079 --> 00:02:06,899
when this comes up you're always going

54
00:02:05,640 --> 00:02:09,300
to be getting this sort of Welcome

55
00:02:06,899 --> 00:02:11,700
window to start with just go ahead and

56
00:02:09,300 --> 00:02:14,220
close that by hitting that X and it'll

57
00:02:11,700 --> 00:02:15,840
also pop open a website button just go

58
00:02:14,220 --> 00:02:18,780
ahead and close that as well

59
00:02:15,840 --> 00:02:20,879
then you can select create a new C or C++

60
00:02:18,780 --> 00:02:23,940
project right here on the sidebar

61
00:02:20,879 --> 00:02:26,280
you can also find that from file new C

62
00:02:23,940 --> 00:02:28,680
slash C++ project

63
00:02:26,280 --> 00:02:30,780
from there you're going to select a

64
00:02:28,680 --> 00:02:32,340
makefile project despite the fact that

65
00:02:30,780 --> 00:02:35,580
we're not actually going to use make

66
00:02:32,340 --> 00:02:37,500
files and then paste in the same name

67
00:02:35,580 --> 00:02:39,060
that you used for the project for the

68
00:02:37,500 --> 00:02:41,879
project name for our sorry for the

69
00:02:39,060 --> 00:02:44,400
workspace as the project name and then

70
00:02:41,879 --> 00:02:46,620
uncheck the generate source and make

71
00:02:44,400 --> 00:02:48,180
files option and then you can go ahead

72
00:02:46,620 --> 00:02:49,500
and hit finish

73
00:02:48,180 --> 00:02:51,540
now the first thing we're going to

74
00:02:49,500 --> 00:02:54,060
always do when we create a new workspace

75
00:02:51,540 --> 00:02:56,160
is we're going to set some preferences

76
00:02:54,060 --> 00:02:58,680
that we want to be set for all of our

77
00:02:56,160 --> 00:03:01,440
workspaces but which I haven't yet found

78
00:02:58,680 --> 00:03:03,599
a way to set universally within eclipse

79
00:03:01,440 --> 00:03:07,260
so go to your preferences it'll be under

80
00:03:03,599 --> 00:03:10,680
Windows preference for Linux and Windows

81
00:03:07,260 --> 00:03:13,019
and edit preferences for macOS

82
00:03:10,680 --> 00:03:14,819
so we go in there and then at the top

83
00:03:13,019 --> 00:03:16,920
left corner you're going to type

84
00:03:14,819 --> 00:03:20,400
scalability

85
00:03:16,920 --> 00:03:22,980
and select that and then you're going to

86
00:03:20,400 --> 00:03:25,319
uncheck this option for disabling editor

87
00:03:22,980 --> 00:03:27,720
live parsing scalability basically just

88
00:03:25,319 --> 00:03:29,819
has to do with essentially it stops

89
00:03:27,720 --> 00:03:31,739
doing parsing if the file gets too big

90
00:03:29,819 --> 00:03:33,720
but we don't want that to be the case

91
00:03:31,739 --> 00:03:36,120
just in case we happen to run into some

92
00:03:33,720 --> 00:03:39,300
files that are extremely big so we're

93
00:03:36,120 --> 00:03:41,040
going to set this to five nines so that

94
00:03:39,300 --> 00:03:43,019
basically it's not going to come into

95
00:03:41,040 --> 00:03:44,819
effect except for extremely extremely

96
00:03:43,019 --> 00:03:46,620
large files and then we're going to go

97
00:03:44,819 --> 00:03:50,760
ahead and hit apply

98
00:03:46,620 --> 00:03:53,879
next in the top left corner type folding

99
00:03:50,760 --> 00:03:56,879
and select C C++ editor folding

100
00:03:53,879 --> 00:03:59,280
option and here I want you to select the

101
00:03:56,879 --> 00:04:01,260
enable folding on preprocessor branches

102
00:03:59,280 --> 00:04:02,459
and enable folding of control flow

103
00:04:01,260 --> 00:04:04,920
statements

104
00:04:02,459 --> 00:04:07,560
so this is the kind of thing that I

105
00:04:04,920 --> 00:04:10,920
mentioned in my anecdote on the website

106
00:04:07,560 --> 00:04:13,620
that I frequently would encounter code

107
00:04:10,920 --> 00:04:16,019
that had lots of if statements with

108
00:04:13,620 --> 00:04:19,019
preprocessors and so the combination of

109
00:04:16,019 --> 00:04:22,019
enable folding plus disabling or hiding

110
00:04:19,019 --> 00:04:24,240
or folding the inactive preprocessor

111
00:04:22,019 --> 00:04:25,740
branches was extremely helpful to hide a

112
00:04:24,240 --> 00:04:27,780
bunch of code that was not relevant to

113
00:04:25,740 --> 00:04:30,540
me so I'm not going to set this for now

114
00:04:27,780 --> 00:04:32,759
but you can if you want but basically

115
00:04:30,540 --> 00:04:34,979
just make sure you set these so that you

116
00:04:32,759 --> 00:04:37,500
have the option of folding the

117
00:04:34,979 --> 00:04:39,840
preprocessor branches and if statements

118
00:04:37,500 --> 00:04:42,240
as well if you want so hit apply and

119
00:04:39,840 --> 00:04:44,759
then close now we need to actually add

120
00:04:42,240 --> 00:04:47,040
some code there so this is where I had

121
00:04:44,759 --> 00:04:49,259
my code checked out and actually let me

122
00:04:47,040 --> 00:04:51,560
quickly check out the actual vulnerable

123
00:04:49,259 --> 00:04:51,560
version

124
00:04:52,560 --> 00:04:58,440
okay now I'm going to go to my location

125
00:04:55,919 --> 00:05:00,600
where I have checked that out which is

126
00:04:58,440 --> 00:05:02,040
just my home directory in this case I'm

127
00:05:00,600 --> 00:05:04,620
going to go back into eclipse and I'm

128
00:05:02,040 --> 00:05:07,020
going to drag and drop that QEMU folder

129
00:05:04,620 --> 00:05:10,500
I've checked out code over this folder

130
00:05:07,020 --> 00:05:12,180
in the project Explorer in the workspace

131
00:05:10,500 --> 00:05:14,100
now when you do that you're going to

132
00:05:12,180 --> 00:05:16,979
have three different options you can

133
00:05:14,100 --> 00:05:18,600
either copy all of these files into the

134
00:05:16,979 --> 00:05:20,880
actual workspace location which again

135
00:05:18,600 --> 00:05:24,539
was the home directory Clips Dash

136
00:05:20,880 --> 00:05:26,580
workspaces and then this name so you can

137
00:05:24,539 --> 00:05:29,039
copy everything you can link to things

138
00:05:26,580 --> 00:05:30,000
or you can link and recreate the

139
00:05:29,039 --> 00:05:33,600
structure

140
00:05:30,000 --> 00:05:34,860
so in order of speed this is the slowest

141
00:05:33,600 --> 00:05:36,479
one because it's essentially going to

142
00:05:34,860 --> 00:05:38,699
iterate through all those files and

143
00:05:36,479 --> 00:05:42,120
create virtual folders and file links

144
00:05:38,699 --> 00:05:44,400
for on a per file basis whereas this is

145
00:05:42,120 --> 00:05:45,840
the fastest one if you just link to the

146
00:05:44,400 --> 00:05:47,639
files and leave them all in place and

147
00:05:45,840 --> 00:05:49,919
don't have to do any file copies then

148
00:05:47,639 --> 00:05:51,960
that's going to be extremely fast and

149
00:05:49,919 --> 00:05:54,360
this is the intermediate thing so if you

150
00:05:51,960 --> 00:05:56,580
want to copy all the files in as they

151
00:05:54,360 --> 00:05:58,680
exist at this moment in time then you

152
00:05:56,580 --> 00:06:00,479
can do that so sometimes I use this

153
00:05:58,680 --> 00:06:02,160
first option if I just want like a

154
00:06:00,479 --> 00:06:05,039
static snapshot that's not going to

155
00:06:02,160 --> 00:06:07,280
change at all sometimes I use this

156
00:06:05,039 --> 00:06:10,620
option the the link to files and folders

157
00:06:07,280 --> 00:06:13,080
if I am going to want to do you know git

158
00:06:10,620 --> 00:06:15,180
updates and have the code move forwards

159
00:06:13,080 --> 00:06:17,940
and backwards and I tend to never use

160
00:06:15,180 --> 00:06:19,440
this third option because it puts a

161
00:06:17,940 --> 00:06:22,080
bunch of extra overhead before you can

162
00:06:19,440 --> 00:06:24,300
even get started so for now I would

163
00:06:22,080 --> 00:06:26,160
recommend you use this link to files and

164
00:06:24,300 --> 00:06:27,120
folders option because that is the

165
00:06:26,160 --> 00:06:29,220
fastest

166
00:06:27,120 --> 00:06:31,380
so you go ahead and do that and what you

167
00:06:29,220 --> 00:06:33,539
should see is that the indexer

168
00:06:31,380 --> 00:06:36,840
immediately starts chewing on the code

169
00:06:33,539 --> 00:06:38,520
down here so the indexer is basically

170
00:06:36,840 --> 00:06:41,100
looking through the code trying to

171
00:06:38,520 --> 00:06:43,319
determine what the definitions of

172
00:06:41,100 --> 00:06:44,699
functions are where the Declarations are

173
00:06:43,319 --> 00:06:47,160
versus the definition what's the

174
00:06:44,699 --> 00:06:49,500
definition of various structs all that

175
00:06:47,160 --> 00:06:52,560
kind of thing so you need to wait until

176
00:06:49,500 --> 00:06:54,240
your indexer actually completes before

177
00:06:52,560 --> 00:06:56,400
you go navigating around the code

178
00:06:54,240 --> 00:06:58,440
otherwise you may get incorrect search

179
00:06:56,400 --> 00:06:59,880
results and things like that so I'm just

180
00:06:58,440 --> 00:07:01,860
going to go ahead and let this complete

181
00:06:59,880 --> 00:07:04,319
and then speed up the video to finish

182
00:07:01,860 --> 00:07:06,180
that off and in this particular case it

183
00:07:04,319 --> 00:07:08,160
looks like the indexer encountered an

184
00:07:06,180 --> 00:07:10,680
error but that's okay I'm just going to

185
00:07:08,160 --> 00:07:12,840
say all right to that for now and

186
00:07:10,680 --> 00:07:15,539
continue on so the last thing I want to

187
00:07:12,840 --> 00:07:17,880
show you here before I get into actual

188
00:07:15,539 --> 00:07:20,759
details of the commands that I think are

189
00:07:17,880 --> 00:07:22,940
the most useful is that when it comes to

190
00:07:20,759 --> 00:07:26,099
analyzing a particular project

191
00:07:22,940 --> 00:07:28,500
oftentimes it is desirable to set some

192
00:07:26,099 --> 00:07:30,960
particular macros or preprocessor

193
00:07:28,500 --> 00:07:33,060
definitions so to do that you don't go

194
00:07:30,960 --> 00:07:35,520
to the preferences instead on the

195
00:07:33,060 --> 00:07:37,500
individual project you right click and

196
00:07:35,520 --> 00:07:39,960
you go to Properties or you can hit Alt

197
00:07:37,500 --> 00:07:43,020
Enter so you go to the properties and

198
00:07:39,960 --> 00:07:45,300
then you go to the C C++ include

199
00:07:43,020 --> 00:07:47,639
path and then right here the very first

200
00:07:45,300 --> 00:07:49,560
option there are things where you can

201
00:07:47,639 --> 00:07:52,199
include include paths if for instance

202
00:07:49,560 --> 00:07:54,300
your source code you can't find some

203
00:07:52,199 --> 00:07:56,160
header files or something like that so

204
00:07:54,300 --> 00:07:59,039
all you have to do is go click on ADD

205
00:07:56,160 --> 00:08:01,319
preprocessor definition and do foo

206
00:07:59,039 --> 00:08:03,360
equals you know one or bar or whatever

207
00:08:01,319 --> 00:08:06,539
it should be set to when you you do that

208
00:08:03,360 --> 00:08:08,039
that will affect the code so I will show

209
00:08:06,539 --> 00:08:09,539
some more of that once we get into the

210
00:08:08,039 --> 00:08:10,979
code once we navigate around once we

211
00:08:09,539 --> 00:08:13,199
find something we want to actually

212
00:08:10,979 --> 00:08:15,840
change okay and then just to look around

213
00:08:13,199 --> 00:08:18,539
the interface briefly of course once the

214
00:08:15,840 --> 00:08:21,060
code has been copied in here then this

215
00:08:18,539 --> 00:08:23,520
file system structure will look exactly

216
00:08:21,060 --> 00:08:25,139
the same as the actual structure of the

217
00:08:23,520 --> 00:08:27,419
folder that was copied in

218
00:08:25,139 --> 00:08:29,340
and then if you double click on a file

219
00:08:27,419 --> 00:08:31,139
you'll have the file show up here it's

220
00:08:29,340 --> 00:08:33,419
all a tabbed interface so you can

221
00:08:31,139 --> 00:08:34,919
navigate around your tabs you can right

222
00:08:33,419 --> 00:08:37,440
click and you can you know once you

223
00:08:34,919 --> 00:08:39,419
eventually inevitably get too many tabs

224
00:08:37,440 --> 00:08:40,979
you can close all the other tabs besides

225
00:08:39,419 --> 00:08:42,779
the ones you care about in a perfect

226
00:08:40,979 --> 00:08:44,520
world you wouldn't see things like this

227
00:08:42,779 --> 00:08:46,440
where it's saying like oh I can't find

228
00:08:44,520 --> 00:08:48,540
that particular header because if you

229
00:08:46,440 --> 00:08:50,459
have that then inevitably it'll lead to

230
00:08:48,540 --> 00:08:52,080
other errors in other places but

231
00:08:50,459 --> 00:08:54,180
sometimes it doesn't really matter and

232
00:08:52,080 --> 00:08:55,860
you can just keep reading the code just

233
00:08:54,180 --> 00:08:57,899
fine even if some things are not

234
00:08:55,860 --> 00:08:59,880
completely obvious in their definitions

235
00:08:57,899 --> 00:09:02,040
so I want to quickly cover you know some

236
00:08:59,880 --> 00:09:04,380
of these tabs down here this problems

237
00:09:02,040 --> 00:09:06,480
has to do with you know if there are

238
00:09:04,380 --> 00:09:07,920
particular issues in the code we can

239
00:09:06,480 --> 00:09:10,860
close that because we're not dealing

240
00:09:07,920 --> 00:09:12,240
with this to actually compile code tasks

241
00:09:10,860 --> 00:09:14,160
okay you can see it's just a bunch of

242
00:09:12,240 --> 00:09:16,380
fixed me's and to Do's we don't need

243
00:09:14,160 --> 00:09:18,000
that either console we're not going to

244
00:09:16,380 --> 00:09:19,800
be running this stuff so we don't need

245
00:09:18,000 --> 00:09:22,080
console output you can close that

246
00:09:19,800 --> 00:09:24,120
properties we'll just include things

247
00:09:22,080 --> 00:09:26,519
like file paths and stuff like that we

248
00:09:24,120 --> 00:09:28,800
don't need that either and call graph

249
00:09:26,519 --> 00:09:30,839
similarly we don't need but I'm not

250
00:09:28,800 --> 00:09:32,420
going to close that quite yet because I

251
00:09:30,839 --> 00:09:35,399
want there to be at least one tab here

252
00:09:32,420 --> 00:09:38,040
for when we do searching commands later

253
00:09:35,399 --> 00:09:40,260
on the outline over here will be things

254
00:09:38,040 --> 00:09:42,120
like what are the functions that are

255
00:09:40,260 --> 00:09:44,640
available in this particular file

256
00:09:42,120 --> 00:09:46,680
personally I almost never actually look

257
00:09:44,640 --> 00:09:48,600
at this so we're going to go ahead and

258
00:09:46,680 --> 00:09:51,360
close that as well

259
00:09:48,600 --> 00:09:53,940
similarly build targets I never use it

260
00:09:51,360 --> 00:09:55,860
we're not using it for actual code we're

261
00:09:53,940 --> 00:09:58,620
compiling so we can go ahead and get rid

262
00:09:55,860 --> 00:10:01,320
of that all right so go ahead and get

263
00:09:58,620 --> 00:10:03,120
your project set up and then when you

264
00:10:01,320 --> 00:10:04,500
come back and you have it all set up we

265
00:10:03,120 --> 00:10:06,000
will go through some of the commands

266
00:10:04,500 --> 00:10:08,100
that you use for navigating around

267
00:10:06,000 --> 00:10:11,660
source code and understanding and

268
00:10:08,100 --> 00:10:11,660
reading the source code more effectively

