1
00:00:00,380 --> 00:00:05,100
uninitialized data access

2
00:00:01,860 --> 00:00:07,500
vulnerabilities what are they well we

3
00:00:05,100 --> 00:00:09,120
know that memory that is uninitialized

4
00:00:07,500 --> 00:00:11,519
and not set to anything in particular

5
00:00:09,120 --> 00:00:14,040
will retain whatever value it was

6
00:00:11,519 --> 00:00:16,199
previously set to this becomes a

7
00:00:14,040 --> 00:00:19,619
security problem when the previous set

8
00:00:16,199 --> 00:00:21,900
value turns out to be ACID so if the

9
00:00:19,619 --> 00:00:23,760
vulnerable code is using uninitialized

10
00:00:21,900 --> 00:00:25,140
data that turns out to be ACID then it's

11
00:00:23,760 --> 00:00:27,119
going to have all the same sort of

12
00:00:25,140 --> 00:00:30,960
problems that we've seen in the past now

13
00:00:27,119 --> 00:00:33,960
as before me US guy no like lot word

14
00:00:30,960 --> 00:00:36,480
chunk so we're not going to call it

15
00:00:33,960 --> 00:00:37,500
uninitialized data access we're going to

16
00:00:36,480 --> 00:00:40,140
call it

17
00:00:37,500 --> 00:00:42,420
UDA nice and simple two syllables UDA

18
00:00:40,140 --> 00:00:45,180
vulnerabilities so let's see a trivial

19
00:00:42,420 --> 00:00:48,180
example of this on the stack we've got

20
00:00:45,180 --> 00:00:50,219
main which has buf eight characters but

21
00:00:48,180 --> 00:00:52,260
it is initialized to zero so that must

22
00:00:50,219 --> 00:00:54,480
not be the problem and we have int i

23
00:00:52,260 --> 00:00:56,820
which is also initialized to 0x1337

24
00:00:54,480 --> 00:00:58,680
then a check then it prints

25
00:00:56,820 --> 00:01:00,780
out the argc and it checks if it is

26
00:00:58,680 --> 00:01:02,699
greater than one it does a strcpy

27
00:01:00,780 --> 00:01:05,100
which we know is one of those suspicious

28
00:01:02,699 --> 00:01:08,040
functions that we don't like strcpy

29
00:01:05,100 --> 00:01:09,180
of argv[1] into buf well right there we

30
00:01:08,040 --> 00:01:11,340
can see that's going to be a Stack

31
00:01:09,180 --> 00:01:13,080
Overflow potential right but this is the

32
00:01:11,340 --> 00:01:15,540
UDA section so we're not worried about

33
00:01:13,080 --> 00:01:18,180
stack overflows we want to find the UDA

34
00:01:15,540 --> 00:01:20,580
vulnerability okay so it's copying

35
00:01:18,180 --> 00:01:22,680
attack control values into buf and then

36
00:01:20,580 --> 00:01:25,619
it's taking the address of the zeroth

37
00:01:22,680 --> 00:01:27,540
element of buf treating it as a integer

38
00:01:25,619 --> 00:01:29,580
pointer and dereferencing that pointer

39
00:01:27,540 --> 00:01:30,840
and storing the value into i so it's

40
00:01:29,580 --> 00:01:32,820
basically immediately taking whatever

41
00:01:30,840 --> 00:01:35,159
the attack controlled value was and

42
00:01:32,820 --> 00:01:38,460
storing it into i then it checks if the

43
00:01:35,159 --> 00:01:40,380
zeroth element of buffer is non-null if

44
00:01:38,460 --> 00:01:42,900
so it calls this function that I'm

45
00:01:40,380 --> 00:01:46,200
calling the acid_setter_func() passes the

46
00:01:42,900 --> 00:01:49,860
address of i well in acid_setter_func()

47
00:01:46,200 --> 00:01:51,780
int p the int pointer p is immediately

48
00:01:49,860 --> 00:01:54,600
dereferenced and stored into the local

49
00:01:51,780 --> 00:01:56,700
variable i inside of the ACID setter

50
00:01:54,600 --> 00:01:59,100
func scope and then it just prints that

51
00:01:56,700 --> 00:02:01,560
out so okay we know that that value

52
00:01:59,100 --> 00:02:03,360
right there is coming from i i is coming

53
00:02:01,560 --> 00:02:05,700
from buf so whatever this attacker

54
00:02:03,360 --> 00:02:08,459
decides to pass in that is what will be

55
00:02:05,700 --> 00:02:10,259
printed there okay and then when this

56
00:02:08,459 --> 00:02:13,080
returns now it calls another function

57
00:02:10,259 --> 00:02:15,599
that I'm calling the uda_func() this

58
00:02:13,080 --> 00:02:18,360
uda_func() got the same address of i from

59
00:02:15,599 --> 00:02:20,340
the previous scope but here it has a

60
00:02:18,360 --> 00:02:22,739
local variable i and oh no the

61
00:02:20,340 --> 00:02:24,720
programmer forgot to initialize this to

62
00:02:22,739 --> 00:02:28,020
anything it's not initialized and then

63
00:02:24,720 --> 00:02:30,000
it's immediately used so that is an UDA

64
00:02:28,020 --> 00:02:31,860
vulnerability uninitialized data being

65
00:02:30,000 --> 00:02:34,440
used of course this particular one is

66
00:02:31,860 --> 00:02:36,180
not going to lead to any sort of truly

67
00:02:34,440 --> 00:02:37,920
advantageous problem for the attacker

68
00:02:36,180 --> 00:02:40,800
but we just want to show it this is the

69
00:02:37,920 --> 00:02:42,780
trivial example for reference so if you

70
00:02:40,800 --> 00:02:44,879
were to run this on different systems

71
00:02:42,780 --> 00:02:46,980
you'll see different results here I ran

72
00:02:44,879 --> 00:02:49,980
it on an Ubuntu system and you can see

73
00:02:46,980 --> 00:02:52,440
with no parameters the argc is one and

74
00:02:49,980 --> 00:02:54,660
the output value is just some

75
00:02:52,440 --> 00:02:56,640
uninitialized garbage and furthermore

76
00:02:54,660 --> 00:02:59,700
this uninitialized garbage changes

77
00:02:56,640 --> 00:03:02,599
between invocations so call it once you

78
00:02:59,700 --> 00:03:07,860
get 7ffc call it again you get

79
00:03:02,599 --> 00:03:09,959
7ffd 7ffe 7ffe okay so that

80
00:03:07,860 --> 00:03:12,480
uninitialized garbage is just changing

81
00:03:09,959 --> 00:03:15,180
naturally between process invocations

82
00:03:12,480 --> 00:03:17,879
then if the attacker passes in a

83
00:03:15,180 --> 00:03:20,819
attacker-controlled string of A's then

84
00:03:17,879 --> 00:03:23,280
that will lead to 414141 being printed

85
00:03:20,819 --> 00:03:25,800
out because that's capital A furthermore

86
00:03:23,280 --> 00:03:27,780
the attacker could pass some hex string

87
00:03:25,800 --> 00:03:29,640
and guarantee that that hex string will

88
00:03:27,780 --> 00:03:32,280
be printed out as well they could also

89
00:03:29,640 --> 00:03:34,680
pass a null string and then you would

90
00:03:32,280 --> 00:03:36,420
get right back to that situation of

91
00:03:34,680 --> 00:03:38,640
uninitialized garbage being printed out

92
00:03:36,420 --> 00:03:41,340
so it may vary from invocation

93
00:03:38,640 --> 00:03:44,340
invocation on some systems run the same

94
00:03:41,340 --> 00:03:46,260
thing on my Mac and on that one we got

95
00:03:44,340 --> 00:03:47,580
the exact same value printed out every

96
00:03:46,260 --> 00:03:50,099
time when we just ran it with no

97
00:03:47,580 --> 00:03:51,720
parameters and with an empty string it

98
00:03:50,099 --> 00:03:53,580
always printed out one instead of going

99
00:03:51,720 --> 00:03:56,040
back to the uninitialized behavior like

100
00:03:53,580 --> 00:03:57,840
the Ubuntu system did and of course if

101
00:03:56,040 --> 00:04:00,360
you pass in something like four A's

102
00:03:57,840 --> 00:04:02,099
you'll get out four A's so we want to

103
00:04:00,360 --> 00:04:04,379
visualize that quickly and just a

104
00:04:02,099 --> 00:04:06,480
reminder for our stack diagrams from

105
00:04:04,379 --> 00:04:08,640
vulnerabilities 1001 low addresses low

106
00:04:06,480 --> 00:04:11,099
high addresses high least significant

107
00:04:08,640 --> 00:04:13,560
byte to the right most significant byte

108
00:04:11,099 --> 00:04:15,780
to the left and of course stacks

109
00:04:13,560 --> 00:04:17,880
always grow from high addresses towards

110
00:04:15,780 --> 00:04:20,040
low addresses so what we call the stack

111
00:04:17,880 --> 00:04:21,780
bottom is at the top of the diagram and

112
00:04:20,040 --> 00:04:24,780
the stack top is at the bottom of the

113
00:04:21,780 --> 00:04:29,160
diagram yes I know it's fun

114
00:04:24,780 --> 00:04:32,040
cool so invocation of ./uda1 has

115
00:04:29,160 --> 00:04:34,500
something like this argv of zero is

116
00:04:32,040 --> 00:04:36,600
whatever the program name is

117
00:04:34,500 --> 00:04:38,940
so our stack would look like this if we

118
00:04:36,600 --> 00:04:41,040
invoked uda1 from our home directory

119
00:04:38,940 --> 00:04:42,479
then that path would be in argv[0]

120
00:04:41,040 --> 00:04:46,560
and the first thing it did was

121
00:04:42,479 --> 00:04:48,840
initialize buf[8] to 0 and i so buf[8]

122
00:04:46,560 --> 00:04:51,660
and we'll say is there and i is

123
00:04:48,840 --> 00:04:53,580
here and then it does checks it says if

124
00:04:51,660 --> 00:04:55,680
argc greater than one well we didn't pass

125
00:04:53,580 --> 00:04:59,340
any arguments this time so that's false

126
00:04:55,680 --> 00:05:01,080
and it'll skip that if buf[0] is not

127
00:04:59,340 --> 00:05:02,699
equal to zero do something else well

128
00:05:01,080 --> 00:05:04,860
it's equal to zero so it's not going to

129
00:05:02,699 --> 00:05:07,979
do that and then finally it'll call the

130
00:05:04,860 --> 00:05:10,020
uda_func() passing the address of i so the

131
00:05:07,979 --> 00:05:12,419
pointer passed in is going to point at

132
00:05:10,020 --> 00:05:13,680
this i but that's not actually the i

133
00:05:12,419 --> 00:05:15,120
that's going to be used inside the

134
00:05:13,680 --> 00:05:17,820
function it's using its own local

135
00:05:15,120 --> 00:05:20,940
variable and that is uninitialized and

136
00:05:17,820 --> 00:05:22,740
so that is the UDA issue but you know

137
00:05:20,940 --> 00:05:24,960
this in and of itself is not a security

138
00:05:22,740 --> 00:05:27,060
issue what you want to see for it to

139
00:05:24,960 --> 00:05:28,979
become a security issue is that the

140
00:05:27,060 --> 00:05:31,919
uninitialized data gets filled in

141
00:05:28,979 --> 00:05:35,880
somehow by attacker-controlled data so

142
00:05:31,919 --> 00:05:37,680
let's continue on if we passed AAAA into

143
00:05:35,880 --> 00:05:40,320
this instead then that would be there

144
00:05:37,680 --> 00:05:41,580
for argv[1] and of course this is

145
00:05:40,320 --> 00:05:43,620
going to be initialized the same way as

146
00:05:41,580 --> 00:05:45,120
last time now it says if argc is

147
00:05:43,620 --> 00:05:47,580
greater than one and it is because we've

148
00:05:45,120 --> 00:05:50,220
got another argument do the strcpy

149
00:05:47,580 --> 00:05:53,580
of the attacker-controlled argv of 1

150
00:05:50,220 --> 00:05:56,820
into buf and then get the address of

151
00:05:53,580 --> 00:05:59,639
buf[0] and immediately dereference it

152
00:05:56,820 --> 00:06:02,580
and put that value into i

153
00:05:59,639 --> 00:06:05,100
so the strcpy takes the A's and

154
00:06:02,580 --> 00:06:08,039
copies them over buf and then the

155
00:06:05,100 --> 00:06:10,259
immediate dereferencing of the zeroth

156
00:06:08,039 --> 00:06:14,039
element as if it were an integer we'll

157
00:06:10,259 --> 00:06:16,380
set 414141 into I then the address of i

158
00:06:14,039 --> 00:06:18,300
will ultimately be passed okay and

159
00:06:16,380 --> 00:06:20,280
there's Kate telling us that this is the

160
00:06:18,300 --> 00:06:23,819
magic ACID value right

161
00:06:20,280 --> 00:06:25,440
okay so buf[0] is not equal to

162
00:06:23,819 --> 00:06:27,900
zero and therefore it's going to call

163
00:06:25,440 --> 00:06:30,180
the ACID setter function ACID setter

164
00:06:27,900 --> 00:06:32,220
function takes the address of this i and

165
00:06:30,180 --> 00:06:35,280
it passes it as p and immediately

166
00:06:32,220 --> 00:06:37,979
dereferences p and then that is stored

167
00:06:35,280 --> 00:06:39,900
into i so that is setting attacker

168
00:06:37,979 --> 00:06:41,580
controlled value at this location and

169
00:06:39,900 --> 00:06:43,139
printing it out

170
00:06:41,580 --> 00:06:44,699
but of course just printing out an

171
00:06:43,139 --> 00:06:46,979
attacker-controlled value is not

172
00:06:44,699 --> 00:06:49,440
necessarily a security issue what I want

173
00:06:46,979 --> 00:06:51,539
to call attention to is the fact that we

174
00:06:49,440 --> 00:06:54,720
lie to ourselves and we say that when

175
00:06:51,539 --> 00:06:56,639
the ACID setter function returns its

176
00:06:54,720 --> 00:06:59,160
stack frame just completely disappears

177
00:06:56,639 --> 00:07:01,560
and it goes off into imaginary space and

178
00:06:59,160 --> 00:07:03,900
doesn't exist anymore but the truth of

179
00:07:01,560 --> 00:07:06,840
the matter is that that memory still

180
00:07:03,900 --> 00:07:09,180
retains exactly the value that was set

181
00:07:06,840 --> 00:07:11,520
during the ACID setter function so it's

182
00:07:09,180 --> 00:07:13,680
still there behind the scenes ready to

183
00:07:11,520 --> 00:07:16,080
be used by the UDA function that is

184
00:07:13,680 --> 00:07:17,880
using this exact location as

185
00:07:16,080 --> 00:07:20,639
uninitialized data

186
00:07:17,880 --> 00:07:22,560
so then we go back up to main main calls

187
00:07:20,639 --> 00:07:24,539
the uda_func() passes the address of i

188
00:07:22,560 --> 00:07:27,419
which is the address of this doesn't

189
00:07:24,539 --> 00:07:30,720
actually use it uses its uninitialized

190
00:07:27,419 --> 00:07:32,580
local variable i but this is still

191
00:07:30,720 --> 00:07:34,680
technically uninitialized in the sense

192
00:07:32,580 --> 00:07:36,960
of what's going on with the program but

193
00:07:34,680 --> 00:07:40,020
it has been maliciously pre-initialized

194
00:07:36,960 --> 00:07:42,660
by the attacker to an ACID value and so

195
00:07:40,020 --> 00:07:44,400
now this is the security problem the

196
00:07:42,660 --> 00:07:47,039
fact that uninitialized data can

197
00:07:44,400 --> 00:07:49,440
magically become ACID data can cause

198
00:07:47,039 --> 00:07:51,180
problems for programs that are using

199
00:07:49,440 --> 00:07:53,520
uninitialized data

200
00:07:51,180 --> 00:07:55,740
so this is our security issue that we're

201
00:07:53,520 --> 00:07:57,720
going to be looking at in this section

202
00:07:55,740 --> 00:07:59,099
and so while the previous code was not

203
00:07:57,720 --> 00:08:01,080
in and of itself a security

204
00:07:59,099 --> 00:08:03,900
vulnerability what if the code was

205
00:08:01,080 --> 00:08:07,680
changed ever so slightly so that uda_func()

206
00:08:03,900 --> 00:08:10,860
returned the integer i and then

207
00:08:07,680 --> 00:08:12,599
what if uda_func() was called to return a

208
00:08:10,860 --> 00:08:15,060
length that was going to be used in a

209
00:08:12,599 --> 00:08:18,479
memcpy well in that case then we'd be

210
00:08:15,060 --> 00:08:22,440
right back to stack overflows with a

211
00:08:18,479 --> 00:08:24,960
ACID length an ACID source buffer and a

212
00:08:22,440 --> 00:08:27,060
destination that is too small for this

213
00:08:24,960 --> 00:08:28,500
arbitrary sized copy that's about to

214
00:08:27,060 --> 00:08:30,360
occur so that's the kind of

215
00:08:28,500 --> 00:08:33,479
vulnerability that can occur when there

216
00:08:30,360 --> 00:08:35,339
is uninitialized data access or UDA

217
00:08:33,479 --> 00:08:37,860
problem all right one more quick example

218
00:08:35,339 --> 00:08:41,039
using the heap instead of the stack so

219
00:08:37,860 --> 00:08:43,680
here main mallocs a buf1 and a buf2

220
00:08:41,039 --> 00:08:45,360
it takes the address of buf1 and

221
00:08:43,680 --> 00:08:47,279
it puts it into i

222
00:08:45,360 --> 00:08:48,839
and then it's going to print out the

223
00:08:47,279 --> 00:08:50,459
buffers it's going to check you know

224
00:08:48,839 --> 00:08:52,140
again print the argc check if it's

225
00:08:50,459 --> 00:08:53,880
greater than one do the strcpy if

226
00:08:52,140 --> 00:08:56,279
it's greater than one so basically it's

227
00:08:53,880 --> 00:08:58,440
doing the attacker-controlled contents

228
00:08:56,279 --> 00:09:01,380
into buf1 but then it's doing a memset

229
00:08:58,440 --> 00:09:03,899
with the exclamation point just to

230
00:09:01,380 --> 00:09:06,660
initialize buf2 to a fixed value so

231
00:09:03,899 --> 00:09:08,580
that's good and this is potentially you

232
00:09:06,660 --> 00:09:10,740
know feeding ACID through the program

233
00:09:08,580 --> 00:09:13,440
then it checks buf1[0] if

234
00:09:10,740 --> 00:09:15,360
that's non-zero then it calls opt_realloc

235
00:09:13,440 --> 00:09:17,339
and it passes the address of buf1

236
00:09:15,360 --> 00:09:19,920
and buf2 what does opt_realloc

237
00:09:17,339 --> 00:09:22,580
do well it takes that buf1 and

238
00:09:19,920 --> 00:09:25,320
buf2 and it frees them and then it

239
00:09:22,580 --> 00:09:26,820
re-mallocs them and then it just prints

240
00:09:25,320 --> 00:09:29,100
out the new addresses that it's gotten

241
00:09:26,820 --> 00:09:31,140
now I'll just say that based on the

242
00:09:29,100 --> 00:09:32,880
particular Ubuntu system that I was

243
00:09:31,140 --> 00:09:34,080
running on you know and this is the kind

244
00:09:32,880 --> 00:09:35,339
of thing where an attacker would know

245
00:09:34,080 --> 00:09:37,380
what their target is they'd know how

246
00:09:35,339 --> 00:09:40,440
their target behaves well I knew just

247
00:09:37,380 --> 00:09:42,480
experimentally that if I reordered these

248
00:09:40,440 --> 00:09:45,120
and I made it so that I malloc buf2

249
00:09:42,480 --> 00:09:47,640
before buf1 then I would actually get

250
00:09:45,120 --> 00:09:50,459
buf2 back as exactly the same address

251
00:09:47,640 --> 00:09:52,500
as was before so I did that just to make

252
00:09:50,459 --> 00:09:54,480
a point about the fact that you know

253
00:09:52,500 --> 00:09:56,220
when you free stuff and then you malloc

254
00:09:54,480 --> 00:09:58,140
it again depending on the implementation

255
00:09:56,220 --> 00:09:59,940
of the heap you could be getting back

256
00:09:58,140 --> 00:10:02,339
the exact same addresses that you had

257
00:09:59,940 --> 00:10:04,860
before and given the fact that buf1

258
00:10:02,339 --> 00:10:06,959
has ACID in it and buf2 has clean

259
00:10:04,860 --> 00:10:09,080
initialized data in it if I'm getting

260
00:10:06,959 --> 00:10:11,640
back the exact same addresses as before

261
00:10:09,080 --> 00:10:13,019
then you know at least for my diagrams

262
00:10:11,640 --> 00:10:15,420
and my visualization that makes things

263
00:10:13,019 --> 00:10:17,040
simpler but for purposes of knowing

264
00:10:15,420 --> 00:10:19,380
where ACID is going to be flowing in the

265
00:10:17,040 --> 00:10:22,140
program that can be important

266
00:10:19,380 --> 00:10:23,940
so once those reallocations occur and it

267
00:10:22,140 --> 00:10:27,120
just freed it and then it got it again

268
00:10:23,940 --> 00:10:29,700
the key point again is that we have

269
00:10:27,120 --> 00:10:32,100
freed the data and we have re-malloced it

270
00:10:29,700 --> 00:10:34,260
so technically even if we get the exact

271
00:10:32,100 --> 00:10:36,120
same address this is technically

272
00:10:34,260 --> 00:10:38,459
uninitialized data at this point after

273
00:10:36,120 --> 00:10:40,740
it's freed there should be no dependency

274
00:10:38,459 --> 00:10:43,560
whatsoever in the code on what the value

275
00:10:40,740 --> 00:10:45,180
should be furthermore we've freed it and

276
00:10:43,560 --> 00:10:46,440
if this was much more complicated code

277
00:10:45,180 --> 00:10:48,480
the attacker might have had an

278
00:10:46,440 --> 00:10:50,459
opportunity to fill it in with ACID

279
00:10:48,480 --> 00:10:52,019
where the you know clean initialized

280
00:10:50,459 --> 00:10:55,140
data was for instance

281
00:10:52,019 --> 00:10:56,760
okay so once opt_realloc returns then

282
00:10:55,140 --> 00:10:58,560
we're down here and I've got a for loop

283
00:10:56,760 --> 00:11:00,180
where all the point of it is just to

284
00:10:58,560 --> 00:11:03,300
print out the contents of buf1 and

285
00:11:00,180 --> 00:11:05,700
buf2. I'm printing at i because we had

286
00:11:03,300 --> 00:11:08,519
set i equal to buf1's address back

287
00:11:05,700 --> 00:11:10,980
here so print out one integer at a time

288
00:11:08,519 --> 00:11:13,200
values from i and then set i equal to

289
00:11:10,980 --> 00:11:16,380
buf2 and print out one integer at a

290
00:11:13,200 --> 00:11:18,839
time from buf2 and then this is the

291
00:11:16,380 --> 00:11:22,140
UDA vulnerability but then at the end of

292
00:11:18,839 --> 00:11:24,540
the day the important thing is blah so

293
00:11:22,140 --> 00:11:26,399
this right here is the ultimate UDA

294
00:11:24,540 --> 00:11:29,220
vulnerability this is using

295
00:11:26,399 --> 00:11:31,860
uninitialized data because buf1 was

296
00:11:29,220 --> 00:11:34,200
reallocated in here and there was never

297
00:11:31,860 --> 00:11:36,000
any subsequent initialization so any use

298
00:11:34,200 --> 00:11:38,640
of buf1 after that point before

299
00:11:36,000 --> 00:11:40,860
initialization is uninitialized data

300
00:11:38,640 --> 00:11:43,200
access but let's see how this turns from

301
00:11:40,860 --> 00:11:45,779
just plain uninitialized garbage into

302
00:11:43,200 --> 00:11:47,459
ACID okay so this is just the output

303
00:11:45,779 --> 00:11:49,320
that I got on one particular Ubuntu

304
00:11:47,459 --> 00:11:51,300
system you would see different things if

305
00:11:49,320 --> 00:11:53,220
you run it somewhere else but this

306
00:11:51,300 --> 00:11:55,980
output right here was the looping over

307
00:11:53,220 --> 00:11:58,620
buf1 and printing out values and this

308
00:11:55,980 --> 00:12:00,360
is the printing out of buf2. so from

309
00:11:58,620 --> 00:12:03,060
this we can see that something weird

310
00:12:00,360 --> 00:12:05,399
happened because we never set zeros into

311
00:12:03,060 --> 00:12:07,980
the first four integer locations in buf1

312
00:12:05,399 --> 00:12:10,200
so something happened there probably

313
00:12:07,980 --> 00:12:11,880
occurring because of the free but what

314
00:12:10,200 --> 00:12:14,519
we can definitely see is that there's

315
00:12:11,880 --> 00:12:15,959
still ACID hanging out in buf1 and

316
00:12:14,519 --> 00:12:20,940
then as I said because of this

317
00:12:15,959 --> 00:12:25,500
invocation of AAA BBB CCC etc. this right

318
00:12:20,940 --> 00:12:27,360
here is going to be EEE FFF GGG so this is

319
00:12:25,500 --> 00:12:30,420
ACID values the attacker-controlled it

320
00:12:27,360 --> 00:12:32,220
based on the argv[1] that they passed in

321
00:12:30,420 --> 00:12:34,459
and now it's still just hanging out

322
00:12:32,220 --> 00:12:37,019
despite the fact that we freed and

323
00:12:34,459 --> 00:12:39,120
malloced again an address

324
00:12:37,019 --> 00:12:40,740
and then similarly we can see that

325
00:12:39,120 --> 00:12:43,980
there's some sort of clobbering going on

326
00:12:40,740 --> 00:12:46,139
for the beginning of buf2 and we can

327
00:12:43,980 --> 00:12:48,240
see that there's clean values that are

328
00:12:46,139 --> 00:12:50,220
still there this is 0x21 is the

329
00:12:48,240 --> 00:12:51,720
exclamation point and at this point I'll

330
00:12:50,220 --> 00:12:54,120
just point out you know a little bit of

331
00:12:51,720 --> 00:12:55,860
a hint of what kind of reuse or

332
00:12:54,120 --> 00:12:58,079
clobbering is going on if we look at

333
00:12:55,860 --> 00:13:03,000
the particulars of this value it's 56

334
00:12:58,079 --> 00:13:04,820
2B 9A CB then we can actually see that 

335
00:13:03,000 --> 00:13:08,100
56 2B

336
00:13:04,820 --> 00:13:11,880
9a cb this looks like it is actually a

337
00:13:08,100 --> 00:13:14,040
pointer to buf1. so somehow buf2 got

338
00:13:11,880 --> 00:13:16,139
a pointer to buf1 in it and again this

339
00:13:14,040 --> 00:13:18,240
all just comes back down to the

340
00:13:16,139 --> 00:13:20,339
particulars of a heap implementation

341
00:13:18,240 --> 00:13:22,560
which an attacker would study and

342
00:13:20,339 --> 00:13:24,360
understand before they tried to do these

343
00:13:22,560 --> 00:13:26,820
sort of vulnerabilities against some

344
00:13:24,360 --> 00:13:30,540
particular target but the end of the day

345
00:13:26,820 --> 00:13:34,860
thing was this printout of buf1[16]

346
00:13:30,540 --> 00:13:37,920
which here is the EEEE and that is the

347
00:13:34,860 --> 00:13:40,320
usage of ACID data that has not been

348
00:13:37,920 --> 00:13:42,240
re-initialized but is just uninitialized

349
00:13:40,320 --> 00:13:43,980
data which under normal circumstances

350
00:13:42,240 --> 00:13:46,320
would be garbage but because the

351
00:13:43,980 --> 00:13:49,200
attacker could control it then now it

352
00:13:46,320 --> 00:13:51,120
becomes ACID okay so reminder our heap

353
00:13:49,200 --> 00:13:53,639
diagrams are the opposite of our stack

354
00:13:51,120 --> 00:13:57,060
diagrams high addresses low low addresses

355
00:13:53,639 --> 00:13:58,380
high big end to the right little end to

356
00:13:57,060 --> 00:14:00,839
the left

357
00:13:58,380 --> 00:14:03,300
so and of course memory writes occur

358
00:14:00,839 --> 00:14:04,740
from low addresses to high addresses so

359
00:14:03,300 --> 00:14:06,540
now we're going to have a diagram that

360
00:14:04,740 --> 00:14:08,100
has both the stack and the heap on it

361
00:14:06,540 --> 00:14:09,899
we've got the stack on the left and

362
00:14:08,100 --> 00:14:11,940
we've got the heap on the right

363
00:14:09,899 --> 00:14:14,279
and so we need to see what's going on

364
00:14:11,940 --> 00:14:17,040
here well first thing we've got malloc

365
00:14:14,279 --> 00:14:19,019
for buf1 and malloc for buf2. so

366
00:14:17,040 --> 00:14:20,700
buf1 and buf2 are local variables

367
00:14:19,019 --> 00:14:22,560
so those are going to show up on our

368
00:14:20,700 --> 00:14:24,360
stack here and buf1 is going to

369
00:14:22,560 --> 00:14:26,100
point somewhere on the heap and buf2

370
00:14:24,360 --> 00:14:29,279
is going to point somewhere on the heap

371
00:14:26,100 --> 00:14:31,860
then the address of buf1 was used for

372
00:14:29,279 --> 00:14:33,480
i and so i will just become a local

373
00:14:31,860 --> 00:14:36,480
variable that points at the same place

374
00:14:33,480 --> 00:14:39,240
as buf1 points then there's the check

375
00:14:36,480 --> 00:14:41,519
if argc is greater than one which it is

376
00:14:39,240 --> 00:14:44,760
here because we're invoking it with AAAA

377
00:14:41,519 --> 00:14:47,760
BBBB and so then it does the strcpy

378
00:14:44,760 --> 00:14:50,339
of all of these A's through F's into

379
00:14:47,760 --> 00:14:53,339
buf1 and then it does the memset of

380
00:14:50,339 --> 00:14:56,100
exclamation point into buf2. so that

381
00:14:53,339 --> 00:14:59,880
looks like this boom initializing all of

382
00:14:56,100 --> 00:15:03,240
this contents from buf1 and buf2 is

383
00:14:59,880 --> 00:15:06,000
initialized to known good data

384
00:15:03,240 --> 00:15:08,639
then there's the call to opt_realloc

385
00:15:06,000 --> 00:15:11,100
and that's going to free buf1 and free

386
00:15:08,639 --> 00:15:12,720
buf2. so the freeing I'm going to say

387
00:15:11,100 --> 00:15:14,699
okay well those things don't point there

388
00:15:12,720 --> 00:15:16,500
anymore later on when we get into the

389
00:15:14,699 --> 00:15:18,300
use after free section we'll learn that

390
00:15:16,500 --> 00:15:20,699
actually they do still point there and

391
00:15:18,300 --> 00:15:24,060
that's a problem but that's not here yet

392
00:15:20,699 --> 00:15:25,800
so buf1 and and buf2 are not

393
00:15:24,060 --> 00:15:27,779
pointing and that memory is you know

394
00:15:25,800 --> 00:15:30,600
theoretically freed and unfortunately

395
00:15:27,779 --> 00:15:32,579
keynote doesn't let me explode this and

396
00:15:30,600 --> 00:15:36,300
make it transparent like it would with

397
00:15:32,579 --> 00:15:37,860
just a normal non-table elements so I'm

398
00:15:36,300 --> 00:15:40,860
just going to leave that there and just

399
00:15:37,860 --> 00:15:43,260
say it got freed but I can't represent

400
00:15:40,860 --> 00:15:44,699
it exactly other than the fact that we

401
00:15:43,260 --> 00:15:47,760
did see that there was some weird

402
00:15:44,699 --> 00:15:50,100
clobbering of the first four integers

403
00:15:47,760 --> 00:15:52,380
worth of space there so something

404
00:15:50,100 --> 00:15:54,480
clobbered that as a side effect of free

405
00:15:52,380 --> 00:15:56,579
and you know that's just the heap

406
00:15:54,480 --> 00:15:58,920
implementation detail and it depends on

407
00:15:56,579 --> 00:16:01,560
the particular heap so those got freed

408
00:15:58,920 --> 00:16:03,959
and they got clobbered but some of the

409
00:16:01,560 --> 00:16:06,660
ACID didn't get clobbered some of the

410
00:16:03,959 --> 00:16:09,480
ACID is still hanging out so when we

411
00:16:06,660 --> 00:16:11,880
call malloc on buf2 and malloc to buf1

412
00:16:09,480 --> 00:16:13,620
we're going to get the pointers back

413
00:16:11,880 --> 00:16:15,360
it turns out they're just you know sort

414
00:16:13,620 --> 00:16:18,180
of allocating them in the opposite order

415
00:16:15,360 --> 00:16:19,860
as they were initially allocated and so

416
00:16:18,180 --> 00:16:21,839
now buf1's back to pointing here and

417
00:16:19,860 --> 00:16:24,120
buf2 is back to pointing there

418
00:16:21,839 --> 00:16:25,920
and then the final thing is at the end

419
00:16:24,120 --> 00:16:28,680
of the day the important thing is blah

420
00:16:25,920 --> 00:16:32,100
and we see that that was using buf1

421
00:16:28,680 --> 00:16:34,199
of offset 16. it's a character array so

422
00:16:32,100 --> 00:16:36,000
it's 16 bytes forward get the address

423
00:16:34,199 --> 00:16:38,399
and immediately dereference that address

424
00:16:36,000 --> 00:16:41,300
as if it was an integer and so that

425
00:16:38,399 --> 00:16:44,940
means that this data that it's using

426
00:16:41,300 --> 00:16:46,680
E's is attacker-controlled data so this is

427
00:16:44,940 --> 00:16:48,540
the UDA vulnerability it was

428
00:16:46,680 --> 00:16:50,399
uninitialized as far as we're concerned

429
00:16:48,540 --> 00:16:52,500
it's just some freshly allocated heap

430
00:16:50,399 --> 00:16:55,259
space that we shouldn't expect anything

431
00:16:52,500 --> 00:16:57,000
about the output but because of course I

432
00:16:55,259 --> 00:16:59,040
knew the way that this was doing I just

433
00:16:57,000 --> 00:17:01,860
simply coded up as a trivial example to

434
00:16:59,040 --> 00:17:03,600
show you the usage of ACID coming off of

435
00:17:01,860 --> 00:17:06,480
the heap because of some prior

436
00:17:03,600 --> 00:17:07,919
allocation and that'll play into the

437
00:17:06,480 --> 00:17:10,020
types of vulnerabilities we'll see in

438
00:17:07,919 --> 00:17:12,059
this section so the most common causes

439
00:17:10,020 --> 00:17:14,579
of UDA vulnerabilities are not

440
00:17:12,059 --> 00:17:16,919
initializing your data when you declare

441
00:17:14,579 --> 00:17:18,839
a local variable on the stack, not

442
00:17:16,919 --> 00:17:21,059
initializing the contents that's handed

443
00:17:18,839 --> 00:17:23,160
back to you from the heap and then

444
00:17:21,059 --> 00:17:25,799
things like using structs but only

445
00:17:23,160 --> 00:17:27,480
partially initializing them so you set a

446
00:17:25,799 --> 00:17:28,980
few values and maybe expect some code

447
00:17:27,480 --> 00:17:31,200
somewhere else to set some other values

448
00:17:28,980 --> 00:17:33,299
but then that code doesn't that plays

449
00:17:31,200 --> 00:17:35,460
into this last bullet here accidental

450
00:17:33,299 --> 00:17:37,500
failure to initialize down an uncommon

451
00:17:35,460 --> 00:17:39,900
control flow path so the most common

452
00:17:37,500 --> 00:17:42,539
place this is seen is that if you have

453
00:17:39,900 --> 00:17:44,280
the allocation of some memory and then

454
00:17:42,539 --> 00:17:46,559
you have like an initializer function

455
00:17:44,280 --> 00:17:48,900
you pass the address of your allocation

456
00:17:46,559 --> 00:17:50,580
into the initializer and you expect okay

457
00:17:48,900 --> 00:17:52,799
initialization function just always

458
00:17:50,580 --> 00:17:55,080
initializes these structs for me but

459
00:17:52,799 --> 00:17:56,940
maybe that initialization function has a

460
00:17:55,080 --> 00:17:59,640
control flow path where it'll return

461
00:17:56,940 --> 00:18:01,860
early and it hasn't initialized

462
00:17:59,640 --> 00:18:03,960
everything then the code that you know

463
00:18:01,860 --> 00:18:06,120
just called the initializer and expects

464
00:18:03,960 --> 00:18:08,400
that it always initializes everything it

465
00:18:06,120 --> 00:18:10,260
just uses that what it assumes is

466
00:18:08,400 --> 00:18:12,600
initialized data that has not actually

467
00:18:10,260 --> 00:18:14,460
been initialized if that's the case then

468
00:18:12,600 --> 00:18:16,919
again we have these situations where if

469
00:18:14,460 --> 00:18:19,380
the attacker could set some contents

470
00:18:16,919 --> 00:18:21,720
into the heap before the allocation that

471
00:18:19,380 --> 00:18:24,179
was then passed into the initializer

472
00:18:21,720 --> 00:18:26,640
then they could potentially have ACID

473
00:18:24,179 --> 00:18:27,720
being used for the uninitialized data so

474
00:18:26,640 --> 00:18:29,340
again we'll see all sorts of

475
00:18:27,720 --> 00:18:31,980
vulnerabilities of this type in this

476
00:18:29,340 --> 00:18:34,980
section so one last thought on the

477
00:18:31,980 --> 00:18:36,600
capability for soft human auditors to

478
00:18:34,980 --> 00:18:39,240
actually find the flaw when it comes to UDA

479
00:18:36,600 --> 00:18:41,760
vulnerabilities is the fact that once

480
00:18:39,240 --> 00:18:43,919
you get to reasonably complicated code

481
00:18:41,760 --> 00:18:46,260
that has lots of indirect control flow

482
00:18:43,919 --> 00:18:48,480
function pointer usage and the like it

483
00:18:46,260 --> 00:18:50,820
becomes much harder for an auditor to

484
00:18:48,480 --> 00:18:53,520
confidently assess whether or not some

485
00:18:50,820 --> 00:18:55,320
particular thing could be UDA or not so

486
00:18:53,520 --> 00:18:57,059
this is why you should start to bring

487
00:18:55,320 --> 00:18:58,620
tools to bear things like memory

488
00:18:57,059 --> 00:19:00,840
sanitizer which you'll learn about later

489
00:18:58,620 --> 00:19:02,880
on or fuzzers which have been mentioned

490
00:19:00,840 --> 00:19:05,280
in previous class as well

491
00:19:02,880 --> 00:19:07,020
and the thing is even once you bring a

492
00:19:05,280 --> 00:19:09,299
tool to bear so the person who's found

493
00:19:07,020 --> 00:19:11,700
the UDA vulnerability has to back trace

494
00:19:09,299 --> 00:19:13,860
it and figure out where the data can be

495
00:19:11,700 --> 00:19:15,840
potentially set to ACID if it's an

496
00:19:13,860 --> 00:19:17,880
attacker of course they have much higher

497
00:19:15,840 --> 00:19:19,980
interest in making sure that they can

498
00:19:17,880 --> 00:19:22,140
actually control that whereas if you're

499
00:19:19,980 --> 00:19:23,460
a defender and you're a programmer you

500
00:19:22,140 --> 00:19:25,500
of course just want to find what's the

501
00:19:23,460 --> 00:19:26,940
right place to initialize it the key

502
00:19:25,500 --> 00:19:29,520
thing is that the best place to

503
00:19:26,940 --> 00:19:31,919
initialize it is upstream ideally where

504
00:19:29,520 --> 00:19:33,419
the thing is first allocated and you

505
00:19:31,919 --> 00:19:35,340
definitely don't want to initialize it

506
00:19:33,419 --> 00:19:37,919
downstream where the actual

507
00:19:35,340 --> 00:19:40,440
vulnerability was found because the

508
00:19:37,919 --> 00:19:42,840
point at which it has actually been used

509
00:19:40,440 --> 00:19:44,760
as uninitialized data means that there

510
00:19:42,840 --> 00:19:46,320
could have been a whole big ACID flow

511
00:19:44,760 --> 00:19:48,360
and you just found yourself on one

512
00:19:46,320 --> 00:19:49,679
particular branch but that ACID could

513
00:19:48,360 --> 00:19:52,080
have flowed elsewhere through the code

514
00:19:49,679 --> 00:19:54,419
and if you fix the problem right close

515
00:19:52,080 --> 00:19:55,679
to the place that it was used then there

516
00:19:54,419 --> 00:19:57,720
could still just be a bunch of other

517
00:19:55,679 --> 00:19:59,160
variants on different control flow paths

518
00:19:57,720 --> 00:20:00,900
you have to figure out where the

519
00:19:59,160 --> 00:20:02,820
allocation is occurring and try to

520
00:20:00,900 --> 00:20:07,400
initialize it as soon as possible after

521
00:20:02,820 --> 00:20:07,400
allocation and as completely as possible

